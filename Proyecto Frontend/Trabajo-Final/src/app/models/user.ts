import { Paciente } from "./paciente";
import { Profesional } from "./profesional";

export interface User{
    email:String;
    token?:String;
    rol?:number;
    password?: string;
    profesional?:Profesional;
    paciente?:Paciente;
    idRol?:number;
    idPersona?:number;
    idUsuario?:number;
}
