export interface Domicilio{
  idDomicilio?: number;
  calle: string;
  nro: string;
  localidad: string;
  provincia: string;
}
