import { Persona } from "./persona";

export interface Profesional{
  idProfesional?: number;
  idUsuario? :number;
  especialidad: string;
  matricula: string;
  estado?:number;
  persona?:Persona;
}
