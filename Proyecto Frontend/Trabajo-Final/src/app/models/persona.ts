import { Domicilio } from "./domicilio";

export interface Persona{
  idPersona?: number;
  idDomicilio?: number;
  dni: string;
  nombre: string;
  apellido: string;
  estado?: number;
  nacionalidad: string;
  telefono: string;
  domicilio:Domicilio;
  fechaNacimiento:Date;
  sexo:string;
}
