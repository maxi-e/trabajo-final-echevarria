import { Persona } from "./persona";

export interface Paciente{
  idPaciente?: number;
  idUsuario?:number;
  idObraSocial?:number;
  nroAfiliado?: string;
  estado?: number;
  persona?:Persona;
}
