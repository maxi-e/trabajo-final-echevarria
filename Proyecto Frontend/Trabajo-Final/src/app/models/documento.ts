export interface Documento{
  idUsuario?:number;
  idTipoDocumento:number;
  nombre:string;
  fechaCreacion?:Date;
  fechaSubida?:Date;
  Formato?:string;
  idInstitucion?:number;
  fichero?:File;
}
