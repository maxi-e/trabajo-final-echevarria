import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent, SafePipe } from './app.component';
import { HomeComponent } from './common/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from './common/layout/layout.component';
import { MaterialModule } from './common/material/material/material.module';
import { HeaderComponent } from './common/navigation/header/header.component';
import { SidenavListComponent } from './common/navigation/sidenav-list/sidenav-list.component';
import { LoginComponent } from './auth/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtInterceptor } from './auth/security/jwt.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RegistarPacienteComponent } from './components/paciente/registar-paciente/registar-paciente.component';
import { RegistarMedicoComponent } from './components/medico/registar-medico/registar-medico.component';
import { HomeMedicoComponent } from './components/medico/home-medico/home-medico.component';
import { HomePacienteComponent } from './components/paciente/home-paciente/home-paciente.component';
import { BuscarPacienteComponent } from './components/medico/buscar-paciente/buscar-paciente.component';
import { DatePipe } from '@angular/common';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { FichaPacienteComponent } from './components/paciente/ficha-paciente/ficha-paciente.component';
import { SubirDocumentoComponent } from './components/paciente/subir-documento/subir-documento.component';
import { TablaDocumentosComponent } from './common/tabla-documentos/tabla-documentos.component';
import { NgxFileDropModule } from 'ngx-file-drop';
import { VisualizarPdfComponent } from './common/visualizar-pdf/visualizar-pdf.component';
import { AutorizacionComponent } from './components/paciente/autorizacion/autorizacion.component';
import { RecuperarContrasenaComponent } from './auth/recuperar-contrasena/recuperar-contrasena.component';
import { BuscarProfesionalComponent } from './components/paciente/buscar-profesional/buscar-profesional.component';
import { EditarPacienteComponent } from './components/paciente/editar-paciente/editar-paciente.component';
import { EditarProfesionalComponent } from './components/medico/editar-profesional/editar-profesional.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LayoutComponent,
    HeaderComponent,
    SidenavListComponent,
    LoginComponent,
    RegistarPacienteComponent,
    RegistarMedicoComponent,
    HomeMedicoComponent,
    HomePacienteComponent,
    BuscarPacienteComponent,
    FichaPacienteComponent,
    SubirDocumentoComponent,
    TablaDocumentosComponent,
    VisualizarPdfComponent,
    SafePipe,
    AutorizacionComponent,
    RecuperarContrasenaComponent,
    BuscarProfesionalComponent,
    EditarPacienteComponent,
    EditarProfesionalComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    NgxFileDropModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    DatePipe,
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
