import { Component, OnInit, Inject } from '@angular/core';
import { DocumentoService } from 'src/app/services/documento.service';
import { saveAs } from 'file-saver';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  idArchivo: number;
}

@Component({
  selector: 'app-visualizar-pdf',
  templateUrl: './visualizar-pdf.component.html',
  styleUrls: ['./visualizar-pdf.component.scss']
})

export class VisualizarPdfComponent implements OnInit {

  archivo:string;
  estado:number = 0;



  constructor(
  private documentoService:DocumentoService,
  @Inject(MAT_DIALOG_DATA) public data:DialogData,
  ) { }

  ngOnInit(): void {
    this.documentoService.getArchivo(this.data.idArchivo).subscribe(res=>{
      var file = new Blob([res], {type: res.type})
      var fileURL = URL.createObjectURL(file);
      this.archivo =  fileURL;
      this.formatoArchivo(res.type)
    });
  }

  formatoArchivo(tipo:string){
    switch(tipo){
      case "application/pdf":{
          this.estado = 1;
          break;
      }
      case "image/png":{
          this.estado = 2;
          break;
        }
      case "image/jpeg":{
          this.estado = 3;
          break;
        }
        case "image/jpg":{
          this.estado = 4;
          break;
        }
      default:{
          this.estado = 5;
          break;
        }
    }
  }





}
