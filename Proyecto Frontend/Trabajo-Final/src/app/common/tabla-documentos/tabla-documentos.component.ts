import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { ObraSocial } from 'src/app/models/obrasocial';
import { Paciente } from 'src/app/models/paciente';
import { DocumentoService } from 'src/app/services/documento.service';
import { ObrasocialService } from 'src/app/services/obrasocial.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { VisualizarPdfComponent } from '../visualizar-pdf/visualizar-pdf.component';
import { eventoService } from 'src/app/services/evento.service';


@Component({
  selector: 'app-tabla-documentos',
  templateUrl: './tabla-documentos.component.html',
  styleUrls: ['./tabla-documentos.component.scss']
})
export class TablaDocumentosComponent implements OnInit {

  pacienteSeleccionado : Paciente;
  //@Input() documentos :MatTableDataSource<any>; //[] = [];
  @Input() tipoDocumento: number;

  displayedColumns: string[] = ['nombre', 'idInstitucion', 'fechaSubida','fechaCreacion','formato','actions'];
  listData:MatTableDataSource<any>;
  obrasSociales:ObraSocial[];
  nombreObraSocial:String;
  readonly width:string='1000';

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort)sort: MatSort;

  constructor(
    private pacienteService:PacienteService,
    private obraSocialService:ObrasocialService,
    private documentoService:DocumentoService,
    public dialog:MatDialog,
    private router:Router,
    private eventoService:eventoService
  ) {
    this.eventoService.$emitter.subscribe(() => {
      this.cargarTabla()
    });
   }

  async ngOnInit(): Promise<void> {
    this.pacienteSeleccionado = this.pacienteService.getPacienteSeleccionado();
    if(this.pacienteSeleccionado == null)
      this.router.navigate(['/home']);
    this.obrasSociales = await (await lastValueFrom(this.obraSocialService.getObrasSociales())).data;
    if(this.pacienteSeleccionado.idObraSocial != null)
      this.nombreObraSocial = this.obrasSociales.find(x=>x.idObraSocial == this.pacienteSeleccionado.idObraSocial).descripcion;
    this.cargarTabla()
  }

  cargarTabla(){
    this.documentoService.getDocumentos(this.pacienteSeleccionado.idUsuario,this.tipoDocumento).subscribe(response =>{
      this.listData= new MatTableDataSource(response.data);
      this.listData.sort = this.sort;
      this.listData.paginator = this.paginator;
    })

  }

  abrir(){
  }
  openView(id:number){
    const dialogRef= this.dialog.open(VisualizarPdfComponent,{
      height: '80%',
      width: '80%',
      data: {idArchivo:id}
    });
    dialogRef.afterClosed().subscribe(result=>{
    });
  }

}
