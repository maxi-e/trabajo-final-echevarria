import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaDocumentosComponent } from './tabla-documentos.component';

describe('TablaDocumentosComponent', () => {
  let component: TablaDocumentosComponent;
  let fixture: ComponentFixture<TablaDocumentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TablaDocumentosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TablaDocumentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
