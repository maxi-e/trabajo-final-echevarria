import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  rol:number;

  constructor() { }


  ngOnInit(): void {
    this.rol = JSON.parse(localStorage.getItem("usuario"))["rol"];
  }

}
