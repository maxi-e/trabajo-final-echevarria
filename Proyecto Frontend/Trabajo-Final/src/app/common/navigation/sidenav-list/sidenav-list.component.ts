import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sidenav-list',
  templateUrl: './sidenav-list.component.html',
  styleUrls: ['./sidenav-list.component.scss']
})
export class SidenavListComponent implements OnInit {

  @Output() sidenavClose = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }
  public onSidenavClose = () => {
    //this.sidenavClose.emit();
  }

//   logout(){
//     this.apiauthService.logout();
//     this.router.navigate(['/home']);
//     this.onSidenavClose();
//     this.snackBar.open('Cierre de sesión exitoso','',{
//       duration:2000
//     });
// }

  //  async mostrarPersona(){
  //   this.id = JSON.parse(localStorage.getItem("usuario"))["userId"];
  //   this.paciente = (await this.personaService.getPersonaUserId(this.id).toPromise()).data;

  //   const dialogRef= this.dialog.open(MostrarPersonaComponent,{
  //     width:'500',
  //     data:this.paciente
  //   });
  //   this.onSidenavClose();
  // }
}
