import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { EditarProfesionalComponent } from 'src/app/components/medico/editar-profesional/editar-profesional.component';
import { EditarPacienteComponent } from 'src/app/components/paciente/editar-paciente/editar-paciente.component';
import { ApiauthService } from 'src/app/services/apiauth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() public sidenavToggle = new EventEmitter();
  readonly width:string='500';

  nombreUsuario:string;
  constructor(public apiAuthService:ApiauthService,
    public apiauthService:ApiauthService,
    public snackBar: MatSnackBar,
    private router:Router,
    public dialog:MatDialog) {
  }

  ngOnInit(): void {
    this.nombreUsuario =JSON.parse(localStorage.getItem("usuario"))["email"];
  }
  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

  cerrarSesion(){
    this.apiAuthService.logout();
    this.router.navigate(['/login']);
    this.snackBar.open('Cierre de sesión exitoso','',{
      duration:2000
    });
  }

  modificarDatos(){
    if(JSON.parse(localStorage.getItem("usuario"))["rol"]==1){
      const dialogRef= this.dialog.open(EditarProfesionalComponent,{
        width:this.width
      });
      dialogRef.afterClosed().subscribe(result=>{
      });
    }else{
      const dialogRef= this.dialog.open(EditarPacienteComponent,{
        width:this.width
      });
      dialogRef.afterClosed().subscribe(result=>{
      });
    }

  }

  // openAdd(){
  //   const dialogRef =this.dialog.open(LoginComponent,{
  //     width:this.width
  //   });
    /*dialogRef.afterClosed().subscribe(result=>{
      this.getVacunas();
    });*/
  //}
}
