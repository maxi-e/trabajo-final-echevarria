import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Url } from './url';
import { Response } from '../models/response';
import { Paciente } from '../models/paciente';
import { Autorizacion } from '../models/autorizacion';

const httpOption={
  headers : new HttpHeaders({
    'Contend-Type':'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class PacienteService {

  url:string;
  paciente:Paciente;

  constructor(
    private _http: HttpClient,
    private _url : Url,
  ) {
    this.url = `${this._url.getUrl()}/paciente`;
   }

  getPacientes():Observable<Response>{
    return this._http.get<Response>(this.url);
  }

  getPaciente(idUsuario:number):Observable<Response>{
    return this._http.get<Response>(`${this.url}/${idUsuario}`);
  }
  getProfesionalesAutorizados():Observable<Response>{
    return this._http.get<Response>(`${this.url}/profesionalesautorizados`);
  }

  setPacienteSeleccionado(paciente:Paciente){
    this.paciente = paciente;
  }

  getPacienteSeleccionado():Paciente{
    return this.paciente;
  }

  addAutorizacion(autorizacion:Autorizacion): Observable<Response>{
    return this._http.post<Response>(`${this.url}/addAutorizacion`, autorizacion, httpOption);
  }

  removerAutorizacion(autorizacion:Autorizacion): Observable<Response>{
    return this._http.put<Response>(`${this.url}/removerAutorizacion`, autorizacion, httpOption);
  }

  updatePaciente(paciente:Paciente):Observable<Response>{
    return this._http.put<Response>(`${this.url}`,paciente, httpOption);
  }

}
