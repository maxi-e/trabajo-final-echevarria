import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Documento } from '../models/documento';
import { Response } from '../models/response';
import { Url } from './url';

const httpOption={
  headers : new HttpHeaders({
    'Contend-Type':'application/json'
  })
};
const httpOption2={
  headers : new HttpHeaders({
    'Contend-Type':'application/json'
  }),responseType: 'blob' as 'json'
};

@Injectable({
  providedIn: 'root'
})
export class DocumentoService {

  url: string;




  constructor(
    private _http: HttpClient,
    private _url : Url,
  ){
    this.url = `${this._url.getUrl()}/documento`;
   }

  getDocumentos(idUsuario:number, idTipoDocumento:number):Observable<Response>{
    return this._http.get<Response>(`${this.url}?idUsuario=${idUsuario}&idTipoDocumento=${idTipoDocumento}`);
  }

  add(documento: Documento): Observable<Response>{
    const formData = new FormData()
     formData.append('nombre', documento.nombre);
     formData.append('idTipoDocumento', documento.idTipoDocumento.toString());
     formData.append('fichero', documento.fichero);
     formData.append('idUsuario', documento.idUsuario.toString());
     formData.append('idInstitucion', documento.idInstitucion.toString());
    console.log(documento)
    return this._http.post<Response>(this.url,formData, httpOption);
  }
  getArchivo(idArchivo:number):Observable<Blob>{
    return this._http.get<Blob>(`${this.url}/soloarchivo/${idArchivo}`, httpOption2);
  }
  // getArchivo(idArchivo:number):Observable<Response>{
  //   return this._http.get<Response>(`${this.url}/archivo/${idArchivo}`);
  // }




}
