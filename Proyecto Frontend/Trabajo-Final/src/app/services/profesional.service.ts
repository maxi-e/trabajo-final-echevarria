import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profesional } from '../models/profesional';
import { Response } from '../models/response';
import { Url } from './url';

const httpOption={
  headers : new HttpHeaders({
    'Contend-Type':'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProfesionalService {

  url:string;

  constructor(
    private _http: HttpClient,
    private _url : Url,
  ) {
    this.url = `${this._url.getUrl()}/profesional`;
   }

  add(profesional: Profesional): Observable<Response>{
    console.log(this._http.post<Response>(this.url, profesional, httpOption));
    return this._http.post<Response>(this.url, profesional, httpOption);
  }

  getProfesionales():Observable<Response>{
    return this._http.get<Response>(this.url);
  }

  getProfesional(id:number):Observable<Response>{
    return this._http.get<Response>(`${this.url}/${id}`);
  }

  updateProfesional(profesional:Profesional):Observable<Response>{
    return this._http.put<Response>(`${this.url}`,profesional, httpOption);
  }


}
