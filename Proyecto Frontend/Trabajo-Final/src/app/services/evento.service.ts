import { EventEmitter, Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class eventoService{

  $emitter = new EventEmitter();

  constructor(){

  }

  emitirEvento() {
      this.$emitter.emit();
  }
}
