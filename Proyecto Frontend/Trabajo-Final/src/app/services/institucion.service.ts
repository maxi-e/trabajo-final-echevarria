import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Url } from './url';
import { Response } from '../models/response';

const httpOption={
  headers : new HttpHeaders({
    'Contend-Type':'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class InstitucionService {

  url: string;


  constructor(
    private _http: HttpClient,
    private _url : Url,
  ) {
    this.url = `${this._url.getUrl()}/institucion`;
   }

   getInstituciones():Observable<Response>{
    return this._http.get<Response>(this.url);
  }

}
