import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Response } from '../models/response';
import { User } from "../models/user";
import { map } from 'rxjs/operators';
import { Login } from "../models/login";
import { Url } from "./url";
import { correo } from "../models/correo";

const httpOption={
    headers : new HttpHeaders({
      'Contend-Type':'application/json'
    })
  };

@Injectable({
    providedIn: 'root'
})

export class ApiauthService{

    url: string;
    private usuarioSubjet : BehaviorSubject<User>;
    public usuario: Observable<User>;


    constructor(private _http: HttpClient,
                private _url : Url)
                {
      this.url= `${this._url.getUrl()}/User`;

      this.usuarioSubjet =
      new BehaviorSubject<User>(JSON.parse(localStorage.getItem('usuario') || null ));
      this.usuario = this.usuarioSubjet.asObservable();
    }

    public get usuarioData(): User{
      return this.usuarioSubjet.value;
    }

    login(login: Login): Observable<Response>{
        return this._http.post<Response>(`${this.url}/login`, login, httpOption).pipe(
          map(res => {
          if(res.exito === 1){
            const usuario: User = res.data;
            localStorage.setItem('usuario',JSON.stringify(usuario));
            this.usuarioSubjet.next(usuario);
          }
          return res;
        })
        );
    }

    logout() {
      localStorage.removeItem('usuario');
      this.usuarioSubjet.next(null);
    }

    addMedico(usuario: User): Observable<Response>{
      return this._http.post<Response>(`${this.url}/registrarMedico`, usuario, httpOption);
    }

    addPaciente(usuario: User): Observable<Response>{
      return this._http.post<Response>(`${this.url}/registrarPaciente`, usuario, httpOption);
    }

    validarEmail(email:correo): Observable<Response>{
      console.log(email);
      return this._http.post<Response>(`${this.url}/validarCorreo`, email, httpOption);
    }

    updatePassword(usuario: User): Observable<Response>{
      debugger
      return this._http.put<Response>(`${this.url}/changePassword`, usuario, httpOption);
    }

}
