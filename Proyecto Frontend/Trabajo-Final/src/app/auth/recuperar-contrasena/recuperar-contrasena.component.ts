import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { ApiauthService } from 'src/app/services/apiauth.service';
import jwt_decode from 'jwt-decode';


@Component({
  selector: 'app-recuperar-contrasena',
  templateUrl: './recuperar-contrasena.component.html',
  styleUrls: ['./recuperar-contrasena.component.scss']
})
export class RecuperarContrasenaComponent implements OnInit {

token: string = "";
email: string = "";
id: number = 0;
password:string = "";
confirmacion:string = "";
usuario:User;
estado: number = 0;


  constructor(private rutaActiva: ActivatedRoute,
    public apiauthService: ApiauthService,
    public snackBar: MatSnackBar,
    private router:Router) { }

ngOnInit(): void {
//debugger
this.token =this.rutaActiva.snapshot.params['tkn'];
console.log(this.token);
var decoded = jwt_decode(this.token);
this.email = decoded["email"];
this.id = decoded["nameid"];
this.usuario={email:decoded["email"], idPersona:decoded["nameid"], password:""};
}

close(){
this.router.navigate(['home']);
}

confirmar(){
if( (this.password != "")&&(this.password == this.confirmacion)){
this.usuario.password = this.password;
this.apiauthService.updatePassword(this.usuario).subscribe(response =>{
if(response.exito ===1){
this.estado = 1;
}else{
this.snackBar.open(response.mensaje,'',{
duration:5000
    });
}
});
}else{
this.snackBar.open('Las contraseña no coincide con la confirmacion','',{
duration:5000
    });
}
}

}
