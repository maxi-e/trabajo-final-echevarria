import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { ApiauthService } from 'src/app/services/apiauth.service';



@Injectable({ providedIn: 'root' })

export class AuthGuard implements CanActivate {

    constructor(private router: Router, private apiauthservice: ApiauthService) {
    }
    canActivate(route: ActivatedRouteSnapshot) {
        const usuario = this.apiauthservice.usuarioData;
        if (usuario && usuario.token) {
            return true;
        }else{
          this.router.navigate(['/login']);
          return false;
        }

    }
}
