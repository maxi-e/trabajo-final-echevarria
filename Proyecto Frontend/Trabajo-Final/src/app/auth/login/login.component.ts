import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, UntypedFormBuilder, UntypedFormGroup, UntypedFormControl, FormControl } from '@angular/forms';
import { ApiauthService } from 'src/app/services/apiauth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Login } from "../../models/login";
import { correo } from 'src/app/models/correo';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


public loginForm = this.formBuilder.group({
  email:['', Validators.required],
  password:['', Validators.required]
})

correo : correo;
loading = false;

constructor(
    public apiauthService: ApiauthService,
    private router: Router,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar
    ){
        if(this.apiauthService.usuarioData){
            this.router.navigate(['/home']);
        }
}
    estado: number = 0;
    Email: string = "";
    //correo : correo;

ngOnInit(){
    this.estado = 0;
}

login() {
this.loading = true;
const credencial:Login={
email:this.loginForm.value.email,
password:this.loginForm.value.password
}
this.apiauthService.login(credencial).subscribe(response =>{
if(response.exito ===1){
this.router.navigate(['/home']);
this.snackBar.open('Inicio de sesión exitoso','',{
    duration:3000
  });
}else{
  this.snackBar.open('Usuario o Contraseña invalida','',{
    duration:5000
    });
}

this.loading = false;
});

}

forgotPassword(){
    this.estado = 1;
}

sendEmail(){
  this.correo={email:this.Email};
  this.apiauthService.validarEmail(this.correo).subscribe(response =>{
      if(response.exito ===1){
          this.snackBar.open('Se envio el correo de recuperacion a su e-mail','',{
              duration:5000
            });
          this.close();
      }else{
          this.snackBar.open('El correo NO se encuentra registrado','',{
              duration:5000
            });
      }
      });
}

close(){
    //this.dialogRef.close();
  }
}
