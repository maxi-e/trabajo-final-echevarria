import { Component } from '@angular/core';
import { User } from './models/user';
import { ApiauthService } from './services/apiauth.service';
import { Pipe, PipeTransform} from "@angular/core";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent{
  title = 'Trabajo-Final';
  opened = false;

  usuario: User;
  constructor(private apiauthService:ApiauthService){
    this.apiauthService.usuario.subscribe(res => {
      this.usuario = res;
    });
  }

}

@Pipe({ name: 'safe' })
  export class SafePipe implements PipeTransform {
    constructor(private sanitizer: DomSanitizer) { }
    transform(url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
  }
