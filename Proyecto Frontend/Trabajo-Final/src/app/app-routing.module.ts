import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RecuperarContrasenaComponent } from './auth/recuperar-contrasena/recuperar-contrasena.component';
import { AuthGuard } from './auth/security/auth.guard';
import { HomeComponent } from './common/home/home.component';
import { BuscarPacienteComponent } from './components/medico/buscar-paciente/buscar-paciente.component';
import { RegistarMedicoComponent } from './components/medico/registar-medico/registar-medico.component';
import { AutorizacionComponent } from './components/paciente/autorizacion/autorizacion.component';
import { FichaPacienteComponent } from './components/paciente/ficha-paciente/ficha-paciente.component';
import { RegistarPacienteComponent } from './components/paciente/registar-paciente/registar-paciente.component';
import { SubirDocumentoComponent } from './components/paciente/subir-documento/subir-documento.component';

const routes: Routes = [
  {path:'home',component:HomeComponent,canActivate:[AuthGuard]},
  {path:'login',component:LoginComponent},
  {path:'registrarMedico',component:RegistarMedicoComponent},
  {path:'registrarPaciente',component:RegistarPacienteComponent},
  {path:'buscarPaciente',component:BuscarPacienteComponent},
  {path:'fichaPaciente',component:FichaPacienteComponent},
  {path:'subirDocumento',component:SubirDocumentoComponent},
  {path:'autorizacion',component:AutorizacionComponent},
  {path: 'recuperarcontraseña/:tkn', component:RecuperarContrasenaComponent},
  {path: '', redirectTo:'/login', pathMatch:'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
