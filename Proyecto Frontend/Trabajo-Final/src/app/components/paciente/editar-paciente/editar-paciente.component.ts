import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Domicilio } from 'src/app/models/domicilio';
import { ObraSocial } from 'src/app/models/obrasocial';
import { Paciente } from 'src/app/models/paciente';
import { Persona } from 'src/app/models/persona';
import { User } from 'src/app/models/user';
import { ApiauthService } from 'src/app/services/apiauth.service';
import { ObrasocialService } from 'src/app/services/obrasocial.service';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-editar-paciente',
  templateUrl: './editar-paciente.component.html',
  styleUrls: ['./editar-paciente.component.scss']
})
export class EditarPacienteComponent implements OnInit {



  startDate = new  Date( 1990 , 0 , 1 );
  user:User;
  paciente:Paciente;
  listaobras: ObraSocial[];


  public registerForm = this.formBuilder.group({
    idPersona:[0],
    dni:['', Validators.required],
    obraElegida:[0, Validators.required],
    nroAfiliado:['', Validators.required],
    apellido:['', Validators.required],
    nombre:['', Validators.required],
    fechaNacimiento:[new Date()],
    sexo:['Femenino', Validators.required],
    domicilioCalle:['', Validators.required],
    domicilioNumero:['', Validators.required],
    localidad:['', Validators.required],
    provincia:['', Validators.required],
    nacionalidad:['', Validators.required],
    telefono:['', Validators.required],
    estado:[true],
  })

  constructor(
    public dialogRef: MatDialogRef<EditarPacienteComponent>,
    private formBuilder: FormBuilder,
    public apiauthService:ApiauthService,
    public snackBar: MatSnackBar,
    private router:Router,
    private pacienteService:PacienteService,
    public obraSocialService:ObrasocialService
    ) {
      if(this.apiauthService.usuarioData){
        this.router.navigate(['/home']);
    }
    }
    estado: number = 0;

    ngOnInit(): void {
      this.getPaciente();
      this.cargarObrasSociales();
  }

  async cargarObrasSociales(){
    this.listaobras = await (await lastValueFrom(this.obraSocialService.getObrasSociales())).data;
  }

  modificarProfesional(){

  }
  volver(){
    this.estado = 0;
  }

  getPaciente(){

    this.pacienteService.getPaciente(JSON.parse(localStorage.getItem("usuario"))["userId"]).subscribe(response=>{
      this.paciente = response.data;
      this.registerForm.controls['domicilioNumero'].setValue(this.paciente.persona.domicilio.calle);
      this.registerForm.controls['domicilioCalle'].setValue(this.paciente.persona.domicilio.nro);
      this.registerForm.controls['localidad'].setValue(this.paciente.persona.domicilio.localidad);
      this.registerForm.controls['provincia'].setValue(this.paciente.persona.domicilio.provincia);
      this.registerForm.controls['dni'].setValue(this.paciente.persona.dni);
      this.registerForm.controls['nombre'].setValue(this.paciente.persona.nombre);
      this.registerForm.controls['apellido'].setValue(this.paciente.persona.apellido);
      this.registerForm.controls['nacionalidad'].setValue(this.paciente.persona.nacionalidad);
      this.registerForm.controls['telefono'].setValue(this.paciente.persona.telefono);
      this.registerForm.controls['fechaNacimiento'].setValue(this.paciente.persona.fechaNacimiento);
      this.registerForm.controls['sexo'].setValue(this.paciente.persona.sexo);
      this.registerForm.controls['obraElegida'].setValue(this.paciente.idObraSocial);
      this.registerForm.controls['nroAfiliado'].setValue(this.paciente.nroAfiliado);
    });
  }

  cancelar(){
    this.dialogRef.close();
  }

  guardar(){
    const domicilioObj:Domicilio={
      calle:this.registerForm.value.domicilioCalle,
      nro:this.registerForm.value.domicilioNumero,
      localidad:this.registerForm.value.localidad,
      provincia:this.registerForm.value.provincia
    }
    const personaObj:Persona={
      dni:this.registerForm.value.dni,
      nombre:this.registerForm.value.nombre,
      apellido:this.registerForm.value.apellido,
      nacionalidad:this.registerForm.value.nacionalidad,
      telefono:this.registerForm.value.telefono,
      domicilio: domicilioObj,
      fechaNacimiento: this.registerForm.value.fechaNacimiento,
      sexo: this.registerForm.value.sexo

    }
    const pacienteObj:Paciente={
      idObraSocial:this.registerForm.value.obraElegida,
      nroAfiliado: this.registerForm.value.nroAfiliado,
      idUsuario:JSON.parse(localStorage.getItem("usuario"))["userId"],
      persona:personaObj,
    }

    this.pacienteService.updatePaciente(pacienteObj).subscribe(response=>{
      if(response.exito===1){
      this.snackBar.open('Se guardaron correctamente los cambios','',{
        duration:2000
      });
      this.dialogRef.close();
    }else{
      this.snackBar.open('Ocurrio un error con el guardado','',{
        duration:2000
      });
    }
  });

  }

}
