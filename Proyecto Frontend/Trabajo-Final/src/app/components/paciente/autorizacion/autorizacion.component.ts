import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Autorizacion } from 'src/app/models/autorizacion';
import { Domicilio } from 'src/app/models/domicilio';
import { Paciente } from 'src/app/models/paciente';
import { Persona } from 'src/app/models/persona';
import { Profesional } from 'src/app/models/profesional';
import { PacienteService } from 'src/app/services/paciente.service';
import { BuscarProfesionalComponent } from '../buscar-profesional/buscar-profesional.component';

@Component({
  selector: 'app-autorizacion',
  templateUrl: './autorizacion.component.html',
  styleUrls: ['./autorizacion.component.scss']
})
export class AutorizacionComponent implements OnInit {

  pacienteSeleccionado : Paciente;
  estado:number=0;
  profesional : Profesional;
  oAutorizacion : Autorizacion={};

  displayedColumns: string[] = ['apellido', 'nombre', 'matricula','especialidad','actions'];
  listData:MatTableDataSource<any>;
  readonly width:string='500';

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort)sort: MatSort;

  listaProfesionales:Profesional[]=[];


  constructor(
    private pacienteService:PacienteService,
    public dialog:MatDialog,
    private router:Router,
    public snackBar: MatSnackBar
    ) { }

  async ngOnInit(): Promise<void> {
    if(this.pacienteService.getPacienteSeleccionado())
    {
      this.estado = 1;
      this.pacienteSeleccionado = this.pacienteService.getPacienteSeleccionado();
      this.getProfesionales();
    }else{
      this.router.navigate(['/home']);
    }
  }

  remover(profesional:Profesional){
    this.oAutorizacion.idUsuarioPaciente = JSON.parse(localStorage.getItem("usuario"))["userId"];
    this.oAutorizacion.idUsuarioProfesional = profesional.idUsuario;
    this.pacienteService.removerAutorizacion(this.oAutorizacion).subscribe(response=>{
    this,this.getProfesionales();
    this.snackBar.open(response.mensaje,'',{
      duration:2000
    });

    });
  }

  getProfesionales(){
    this.pacienteService.getProfesionalesAutorizados().subscribe(response=>{
      this.listData= new MatTableDataSource(response.data);
          this.listData.sort = this.sort;
          this.listData.paginator = this.paginator;
          this.listData.filterPredicate = (data, filter) => {
            return this.displayedColumns.some(ele => {
              return ele != 'actions' && data.persona[ele]?.toLowerCase().indexOf(filter) != -1;
            });
          };
    });
  }

  autorizacion(){
    const dialogRef= this.dialog.open(BuscarProfesionalComponent,{
      width:this.width,
      data:this.pacienteSeleccionado
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.getProfesionales();
    });
  }
}
