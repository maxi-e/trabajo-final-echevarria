import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { lastValueFrom, map } from 'rxjs';
import { Domicilio } from 'src/app/models/domicilio';
import { ObraSocial } from 'src/app/models/obrasocial';
import { Paciente } from 'src/app/models/paciente';
import { Persona } from 'src/app/models/persona';
import { Profesional } from 'src/app/models/profesional';
import { User } from 'src/app/models/user';
import { ApiauthService } from 'src/app/services/apiauth.service';
import { ObrasocialService } from 'src/app/services/obrasocial.service';


@Component({
  selector: 'app-registar-paciente',
  templateUrl: './registar-paciente.component.html',
  styleUrls: ['./registar-paciente.component.scss']
})
export class RegistarPacienteComponent implements OnInit {

  startDate = new  Date( 1990 , 0 , 1 );
  user:User;

  listaobras: ObraSocial[];

  public registerForm = this.formBuilder.group({
    idPersona:[0],
    dni:['', Validators.required],
    obraElegida:[0, Validators.required],
    nroAfiliado:['', Validators.required],
    especialidad:['', Validators.required],
    apellido:['', Validators.required],
    nombre:['', Validators.required],
    fechaNacimiento:[new Date()],
    sexo:['Femenino', Validators.required],
    domicilioCalle:['', Validators.required],
    domicilioNumero:['', Validators.required],
    localidad:['', Validators.required],
    provincia:['', Validators.required],
    nacionalidad:['', Validators.required],
    telefono:['', Validators.required],
    estado:[true],
  })

  public userForm = this.formBuilder.group({
    email:['', Validators.required],
    password:['', Validators.required],
    password2:['', Validators.required]
  })

  constructor(
    private formBuilder: FormBuilder,
    public apiauthService:ApiauthService,
    public obraSocialService:ObrasocialService,
    public snackBar: MatSnackBar,
    private router:Router) {
      if(this.apiauthService.usuarioData){
        this.router.navigate(['/home']);
    }
    this.cargarObrasSociales();
    }

  estado: number = 0;
   ngOnInit():void{
  }

  async cargarObrasSociales(){
    this.listaobras = await (await lastValueFrom(this.obraSocialService.getObrasSociales())).data;
  }

  crearUsuario(){
    this.estado = 1;
  }

  limpiar(){

  }

  volver(){
    this.estado = 0;
  }

  checkPassword(  ){
    if(this.userForm.value.password == this.userForm.value.password2){
      return true;
    }
    return false;
  }

  guardar(){
    if(this.checkPassword()){
    const domicilioObj:Domicilio={
      calle:this.registerForm.value.domicilioCalle,
      nro:this.registerForm.value.domicilioNumero,
      localidad:this.registerForm.value.localidad,
      provincia:this.registerForm.value.provincia
    }
    const personaObj:Persona={
      dni:this.registerForm.value.dni,
      nombre:this.registerForm.value.nombre,
      apellido:this.registerForm.value.apellido,
      nacionalidad:this.registerForm.value.nacionalidad,
      telefono:this.registerForm.value.telefono,
      domicilio: domicilioObj,
      fechaNacimiento: this.registerForm.value.fechaNacimiento,
      sexo: this.registerForm.value.sexo

    }
    const pacienteObj:Paciente={
      idObraSocial:this.registerForm.value.obraElegida,
      nroAfiliado: this.registerForm.value.nroAfiliado,
      persona:personaObj,
    }

    const usuario:User={
      email:this.userForm.value.email,
      password:this.userForm.value.password,
      paciente:pacienteObj,
      idRol:2
    }
    this.apiauthService.addPaciente(usuario).subscribe(response=>{
      if(response.exito===1){
      this.snackBar.open('Paciente creado con éxito','',{
        duration:2000
      });
      this.router.navigate(['login']);
    }else{
      this.snackBar.open('Ya se encuenta registrado un usuario con ese e-mail','',{
        duration:2000
      });
    }
  });
  }else{
    this.snackBar.open('Las contraseñas introducidas no coinciden','',{
      duration:2000
    });
  }
  }
}
