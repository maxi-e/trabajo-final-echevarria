import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NgxFileDropEntry } from 'ngx-file-drop';
import { lastValueFrom } from 'rxjs';
import { Documento } from 'src/app/models/documento';
import { Institucion } from 'src/app/models/institucion';
import { ObraSocial } from 'src/app/models/obrasocial';
import { Paciente } from 'src/app/models/paciente';
import { TipoDocumento } from 'src/app/models/TipoDocumento';
import { DocumentoService } from 'src/app/services/documento.service';
import { InstitucionService } from 'src/app/services/institucion.service';
import { TipoDocumentoService } from 'src/app/services/tipodocumento.service';

@Component({
  selector: 'app-subir-documento',
  templateUrl: './subir-documento.component.html',
  styleUrls: ['./subir-documento.component.scss']
})
export class SubirDocumentoComponent implements OnInit {

  listaobras:ObraSocial[];
  listaInstituciones:Institucion[];
  listaTiposDocumentos:TipoDocumento[];
  public files: NgxFileDropEntry[] = [];
  file:File;
  estado:number = 1;

  public documentoForm = this.formBuilder.group({
    institucionElegida:[0, Validators.required],
    tipoDocumentoElegido:[0, Validators.required],
    nombreDocumento:["", Validators.required]

  })

  constructor(
    private institucionService:InstitucionService,
    private tipoDocumentoService:TipoDocumentoService,
    private documentoService:DocumentoService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<SubirDocumentoComponent>,
    @Inject(MAT_DIALOG_DATA) public pacienteSeleccionado:Paciente
  ) { }

  ngOnInit(): void {
    this.cargarInstituciones();
    this.cargarTiposDocumentos();
    console.log(this.pacienteSeleccionado)
  }

  async cargarInstituciones(){
    this.listaInstituciones = await (await lastValueFrom(this.institucionService.getInstituciones())).data;
    console.log(this.listaInstituciones)
  }

  async cargarTiposDocumentos(){
    this.listaTiposDocumentos = await (await lastValueFrom(this.tipoDocumentoService.getTipoDocumentos())).data;
    console.log(this.listaTiposDocumentos)
  }

  confirmar(){
    for (const droppedFile of this.files) {
    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        this.file=file;
      });

      const documento:Documento={
        nombre:this.documentoForm.value.nombreDocumento,
        idTipoDocumento:this.documentoForm.value.tipoDocumentoElegido,
        idInstitucion:this.documentoForm.value.institucionElegida,
        idUsuario:this.pacienteSeleccionado.idUsuario,
        fichero:this.file
      }
      this.documentoService.add(documento).subscribe(response=>{
        if(response.exito===1){
          this.snackBar.open('El Documento se guardo correctamente','',{
            duration:2000
          });
          this.close();
        }else{
          this.snackBar.open(response.mensaje,'',{
            duration:2000
          });
        }
      });
    }
  }
  }

  close(){
    this.dialogRef.close();
  }


  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {

      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          console.log(droppedFile.relativePath, fileEntry);
          window.open(droppedFile.relativePath);
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
    this.estado = 2;
  }

  public fileOver(event){
    console.log(event);
  }

  public fileLeave(event){
    console.log(event);
  }


}
