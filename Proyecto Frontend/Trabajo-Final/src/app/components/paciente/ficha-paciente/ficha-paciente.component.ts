import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { ObraSocial } from 'src/app/models/obrasocial';
import { Paciente } from 'src/app/models/paciente';
import { eventoService } from 'src/app/services/evento.service';
import { ObrasocialService } from 'src/app/services/obrasocial.service';
import { PacienteService } from 'src/app/services/paciente.service';
import { SubirDocumentoComponent } from '../subir-documento/subir-documento.component';

@Component({
  selector: 'app-ficha-paciente',
  templateUrl: './ficha-paciente.component.html',
  styleUrls: ['./ficha-paciente.component.scss']
})
export class FichaPacienteComponent implements OnInit {

  pacienteSeleccionado : Paciente;
  estado:number=0;


  displayedColumns: string[] = ['nombre', 'institucion', 'fechaSubida','fechaCreacion','formato','actions'];
  listData:MatTableDataSource<any>;
  obrasSociales:ObraSocial[];
  nombreObraSocial:String;
  readonly width:string='500';

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort)sort: MatSort;

  constructor(
    private pacienteService:PacienteService,
    private obraSocialService:ObrasocialService,
    public dialog:MatDialog,
    private router:Router,
    private eventoService:eventoService
    ) { }

  async ngOnInit(): Promise<void> {
    if(this.pacienteService.getPacienteSeleccionado())
    {
      this.estado = 1;
      this.pacienteSeleccionado = this.pacienteService.getPacienteSeleccionado();
      this.obrasSociales = await (await lastValueFrom(this.obraSocialService.getObrasSociales())).data;
      if(this.pacienteSeleccionado.idObraSocial != null)
        this.nombreObraSocial = this.obrasSociales.find(x=>x.idObraSocial == this.pacienteSeleccionado.idObraSocial).descripcion;
    }else{
      this.router.navigate(['/home']);
    }
  }

  abrir(){}

  nuevoDocumento(){
    const dialogRef= this.dialog.open(SubirDocumentoComponent,{
      width:this.width,
      data:this.pacienteSeleccionado
    });
    dialogRef.afterClosed().subscribe(result=>{
      this.eventoService.emitirEvento();
    });
  }
}
