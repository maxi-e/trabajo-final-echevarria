import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Autorizacion } from 'src/app/models/autorizacion';
import { Domicilio } from 'src/app/models/domicilio';
import { Persona } from 'src/app/models/persona';
import { Profesional } from 'src/app/models/profesional';
import { PacienteService } from 'src/app/services/paciente.service';
import { ProfesionalService } from 'src/app/services/profesional.service';

@Component({
  selector: 'app-buscar-profesional',
  templateUrl: './buscar-profesional.component.html',
  styleUrls: ['./buscar-profesional.component.scss']
})
export class BuscarProfesionalComponent implements OnInit {

  displayedColumns: string[] = ['apellido', 'nombre', 'matricula','especialidad','actions'];
  listData:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort)sort: MatSort;

  listaProfesionales:Profesional[]=[];

  searchKey:string = "";
  //pacienteSeleccionado:Paciente;
  profesionalSeleccionado:Profesional
  estado:number;
  autorizacion:Autorizacion = {};

  constructor(
    public dialogRef: MatDialogRef<BuscarProfesionalComponent>,
    private profesionalService:ProfesionalService,
    private pacientesService:PacienteService,
    private router:Router,
    public snackBar: MatSnackBar) {

   }

  ngOnInit(): void {
    this.estado = 1;
    this.getProfesionales();
  }

  getProfesionales(){
    this.profesionalService.getProfesionales().subscribe(response =>{
          this.listData= new MatTableDataSource(response.data);
          this.listData.sort = this.sort;
          this.listData.paginator = this.paginator;
          this.listData.filterPredicate = (data, filter) => {
            return this.displayedColumns.some(ele => {
              return ele != 'actions' && data.persona[ele]?.toLowerCase().indexOf(filter) != -1;
            });
          };
        })
  }

  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  seleccionar(profesional:Profesional){
    this.listData.data.splice(this.listData.data.indexOf(profesional),1);
    this.profesionalSeleccionado = profesional;
    this.estado = 2;
  }


  confirmar(){
    this.autorizacion.idUsuarioPaciente = JSON.parse(localStorage.getItem("usuario"))["userId"];
    this.autorizacion.idUsuarioProfesional = this.profesionalSeleccionado.idUsuario;
    this.pacientesService.addAutorizacion(this.autorizacion).subscribe(response=>{
      this.close();
      this.snackBar.open(response.mensaje,'',{
        duration:2000
      });
    });
  }

  close(){
    this.dialogRef.close();
  }
}
