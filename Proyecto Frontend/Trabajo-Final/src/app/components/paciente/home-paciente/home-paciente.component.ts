import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom } from 'rxjs';
import { Paciente } from 'src/app/models/paciente';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-home-paciente',
  templateUrl: './home-paciente.component.html',
  styleUrls: ['./home-paciente.component.scss']
})
export class HomePacienteComponent implements OnInit {

  idUsuario:number;


  constructor(
    private pacienteService:PacienteService,
    private router:Router
  ) { }

  ngOnInit(): void {
  }

  async consulta(){
    this.idUsuario = JSON.parse(localStorage.getItem("usuario"))["userId"];
    let paciente = await (await lastValueFrom(this.pacienteService.getPaciente(this.idUsuario))).data;
    this.pacienteService.setPacienteSeleccionado(paciente);
    this.router.navigate(['fichaPaciente']);
  }

  async autorizacion(){
    this.idUsuario = JSON.parse(localStorage.getItem("usuario"))["userId"];
    let paciente = await (await lastValueFrom(this.pacienteService.getPaciente(this.idUsuario))).data;
    this.pacienteService.setPacienteSeleccionado(paciente);
    this.router.navigate(['autorizacion']);
  }

}
