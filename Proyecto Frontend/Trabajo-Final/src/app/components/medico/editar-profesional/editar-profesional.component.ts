import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Domicilio } from 'src/app/models/domicilio';
import { Persona } from 'src/app/models/persona';
import { Profesional } from 'src/app/models/profesional';
import { User } from 'src/app/models/user';
import { ApiauthService } from 'src/app/services/apiauth.service';
import { ProfesionalService } from 'src/app/services/profesional.service';

@Component({
  selector: 'app-editar-profesional',
  templateUrl: './editar-profesional.component.html',
  styleUrls: ['./editar-profesional.component.scss']
})
export class EditarProfesionalComponent implements OnInit {


  startDate = new  Date( 1990 , 0 , 1 );
  user:User;
  profesional:Profesional;

  public registerForm = this.formBuilder.group({
    idPersona:[0],
    dni:['', Validators.required],
    matricula:['', Validators.required],
    especialidad:['', Validators.required],
    apellido:['', Validators.required],
    nombre:['', Validators.required],
    fechaNacimiento:[new Date()],
    sexo:['Femenino', Validators.required],
    domicilioCalle:['', Validators.required],
    domicilioNumero:['', Validators.required],
    localidad:['', Validators.required],
    provincia:['', Validators.required],
    nacionalidad:['', Validators.required],
    telefono:['', Validators.required],
    estado:[true],
  })

  constructor(
    public dialogRef: MatDialogRef<EditarProfesionalComponent>,
    private formBuilder: FormBuilder,
    public apiauthService:ApiauthService,
    public snackBar: MatSnackBar,
    private router:Router,
    private profesionalService:ProfesionalService) {
      if(this.apiauthService.usuarioData){
        this.router.navigate(['/home']);
    }
    }
    estado: number = 0;

    ngOnInit(): void {
      this.getProfesional();
  }


  modificarProfesional(){

  }
  volver(){
    this.estado = 0;
  }

  getProfesional(){
    this.profesionalService.getProfesional(JSON.parse(localStorage.getItem("usuario"))["userId"]).subscribe(response=>{
      this.profesional = response.data;
      this.registerForm.controls['domicilioNumero'].setValue(this.profesional.persona.domicilio.calle);
      this.registerForm.controls['domicilioCalle'].setValue(this.profesional.persona.domicilio.nro);
      this.registerForm.controls['localidad'].setValue(this.profesional.persona.domicilio.localidad);
      this.registerForm.controls['provincia'].setValue(this.profesional.persona.domicilio.provincia);
      this.registerForm.controls['dni'].setValue(this.profesional.persona.dni);
      this.registerForm.controls['nombre'].setValue(this.profesional.persona.nombre);
      this.registerForm.controls['apellido'].setValue(this.profesional.persona.apellido);
      this.registerForm.controls['nacionalidad'].setValue(this.profesional.persona.nacionalidad);
      this.registerForm.controls['telefono'].setValue(this.profesional.persona.telefono);
      this.registerForm.controls['fechaNacimiento'].setValue(this.profesional.persona.fechaNacimiento);
      this.registerForm.controls['sexo'].setValue(this.profesional.persona.sexo);
      this.registerForm.controls['especialidad'].setValue(this.profesional.especialidad);
      this.registerForm.controls['matricula'].setValue(this.profesional.matricula);
    });
  }

  cancelar(){
    this.dialogRef.close();
  }

  guardar(){
    const domicilioObj:Domicilio={
      calle:this.registerForm.value.domicilioCalle,
      nro:this.registerForm.value.domicilioNumero,
      localidad:this.registerForm.value.localidad,
      provincia:this.registerForm.value.provincia
    }
    const personaObj:Persona={
      dni:this.registerForm.value.dni,
      nombre:this.registerForm.value.nombre,
      apellido:this.registerForm.value.apellido,
      nacionalidad:this.registerForm.value.nacionalidad,
      telefono:this.registerForm.value.telefono,
      domicilio: domicilioObj,
      fechaNacimiento: this.registerForm.value.fechaNacimiento,
      sexo: this.registerForm.value.sexo

    }
    const profesionalObj:Profesional={
      especialidad:this.registerForm.value.especialidad,
      matricula:this.registerForm.value.matricula,
      persona:personaObj,
      idUsuario:JSON.parse(localStorage.getItem("usuario"))["userId"]
    }

    this.profesionalService.updateProfesional(profesionalObj).subscribe(response=>{
      if(response.exito===1){
      this.snackBar.open('Se guardaron correctamente los cambios','',{
        duration:2000
      });
      this.dialogRef.close();
    }else{
      this.snackBar.open('Ocurrio un error con el guardado','',{
        duration:2000
      });
    }
  });

  }
}
