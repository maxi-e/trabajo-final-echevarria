import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Paciente } from 'src/app/models/paciente';
import { BuscarPacienteComponent } from '../buscar-paciente/buscar-paciente.component';

@Component({
  selector: 'app-home-medico',
  templateUrl: './home-medico.component.html',
  styleUrls: ['./home-medico.component.scss']
})
export class HomeMedicoComponent implements OnInit {

  readonly width:string='500';

  constructor(public dialog:MatDialog) { }

  ngOnInit(): void {
  }

  openView(){
    const dialogRef= this.dialog.open(BuscarPacienteComponent,{
      width:this.width
    });
    dialogRef.afterClosed().subscribe(result=>{
    });
  }


}
