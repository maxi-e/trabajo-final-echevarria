import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/models/paciente';
import { PacienteService } from 'src/app/services/paciente.service';

@Component({
  selector: 'app-buscar-paciente',
  templateUrl: './buscar-paciente.component.html',
  styleUrls: ['./buscar-paciente.component.scss']
})
export class BuscarPacienteComponent implements OnInit {

  displayedColumns: string[] = ['apellido', 'nombre', 'dni','actions'];
  listData:MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator:MatPaginator;
  @ViewChild(MatSort)sort: MatSort;

  searchKey:string = "";
  pacienteSeleccionado:Paciente;
  estado:number;

  constructor(
    public dialogRef: MatDialogRef<BuscarPacienteComponent>,
    private pacienteService:PacienteService,
    private router:Router) {

   }

  ngOnInit(): void {
    this.estado = 1;
    this.getPacientes();
  }

  getPacientes(){
    this.pacienteService.getPacientes().subscribe(response =>{
      this.listData= new MatTableDataSource(response.data);
      this.listData.sort = this.sort;
      this.listData.paginator = this.paginator;
      this.listData.filterPredicate = (data, filter) => {
        return this.displayedColumns.some(ele => {
          return ele != 'actions' && data.persona[ele]?.toLowerCase().indexOf(filter) != -1;
        });
      };
    })
  }


  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  seleccionar(paciente:Paciente){
    this.listData.data.splice(this.listData.data.indexOf(paciente),1);
    this.pacienteSeleccionado = paciente;
    this.estado = 2;
  }


  confirmar(){
    this.pacienteService.setPacienteSeleccionado(this.pacienteSeleccionado);
    this.router.navigate(['fichaPaciente']);
    this.close();
  }

  close(){
    this.dialogRef.close();
  }

}
