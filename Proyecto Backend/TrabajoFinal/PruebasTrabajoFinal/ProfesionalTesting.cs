﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Controllers;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebasTrabajoFinal
{
    public class ProfesionalTesting
    {
        private readonly IProfesionalServices _service;
        private readonly ProfesionalController _controller;

        public ProfesionalTesting()
        {
            _service = new ProfesionalService();
            _controller = new ProfesionalController(_service);
        }
        
        [Fact]
        public void GetListaPacientes_OK()
        {
            var result = _controller.Get();

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void GetListaPacientes_ListaNoNula()
        {
            var result = (OkObjectResult)_controller.Get();
            var respuesta = Assert.IsType<Respuesta>(result.Value);
            List<ProfesionalResponse> prueba = (List<ProfesionalResponse>)respuesta.Data;
            Assert.True(prueba.Count>0);
        }


        [Fact]
        public void GetPacientePorId_OK()
        {
            var result = (OkObjectResult)_controller.GetProfesionalId(11);
            var respuesta = Assert.IsType<Respuesta>(result.Value);
            Assert.True(respuesta.Exito == 1);
        }

        [Fact]
        public void GetPacientePorId_Error()
        {
            var result = (OkObjectResult)_controller.GetProfesionalId(50);
            var respuesta = Assert.IsType<Respuesta>(result.Value);
            Assert.True(respuesta.Exito == 0);
        }


    }
}
