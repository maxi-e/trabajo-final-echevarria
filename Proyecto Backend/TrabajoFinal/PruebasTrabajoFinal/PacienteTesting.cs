﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Controllers;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebasTrabajoFinal
{
    public class PacienteTesting
    {
        private readonly IPacienteService _service;
        private readonly PacienteController _controller;

        public PacienteTesting() {
            _service = new PacienteService();
            _controller = new PacienteController(_service);
        }

        [Fact]
        public void GetPacientePorId_OK()
        {
            var result = _controller.GetPacienteId(4);

            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void addAutorizacion_OK() 
        {
            AutorizacionRequest autorizacion = new AutorizacionRequest();
            autorizacion.IdUsuarioPaciente = 15;
            autorizacion.IdUsuarioProfesional = 12;
            var result = _controller.Add(autorizacion);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void addAutorizacion_Incorrecta() // Uno o ambos de los IDs no se encuentran registrados
        {
            AutorizacionRequest autorizacion = new AutorizacionRequest();
            autorizacion.IdUsuarioPaciente = 19;
            autorizacion.IdUsuarioProfesional = 12;
            var result = (OkObjectResult)_controller.Add(autorizacion);
            var respuesta = Assert.IsType<Respuesta>(result.Value);
            Assert.True(respuesta.Exito == 0);
        }

        [Fact]
        public void crearNuevaAutorizacion() // Creacion de la nueva relacion
        {
            AutorizacionRequest autorizacion = new AutorizacionRequest();
            autorizacion.IdUsuarioPaciente = 16;
            autorizacion.IdUsuarioProfesional = 10;
            var result = (OkObjectResult)_controller.Add(autorizacion);
            var respuesta = Assert.IsType<Respuesta>(result.Value);
            Assert.True(respuesta.Mensaje == "Se creo el permiso exitosamente");
        }

        [Fact]
        public void renovarAutorizacion() // Actualizacion de la relacion existente
        {
            AutorizacionRequest autorizacion = new AutorizacionRequest();
            autorizacion.IdUsuarioPaciente = 16;
            autorizacion.IdUsuarioProfesional = 12;
            var result = (OkObjectResult)_controller.Add(autorizacion);
            var respuesta = Assert.IsType<Respuesta>(result.Value);
            Assert.True(respuesta.Mensaje == "Se actualizo el permiso exitosamente");
        }

        [Fact]
        public void removerAutorizacion_Ok() 
        {
            AutorizacionRequest autorizacion = new AutorizacionRequest();
            autorizacion.IdUsuarioPaciente = 16;
            autorizacion.IdUsuarioProfesional = 12;

            var result = (OkObjectResult)_controller.Edit(autorizacion);
            var respuesta = Assert.IsType<Respuesta>(result.Value);

            Assert.True(respuesta.Mensaje == "Se quitaron los permisos otorgados");
        }

        [Fact]
        public void removerAutorizacion_Error()
        {
            AutorizacionRequest autorizacion = new AutorizacionRequest();
            autorizacion.IdUsuarioPaciente = 16;
            autorizacion.IdUsuarioProfesional = 22;

            var result = (OkObjectResult)_controller.Edit(autorizacion); // relacion inexistente
            var respuesta = Assert.IsType<Respuesta>(result.Value);

            Assert.True(respuesta.Exito == 0);
        }

    }
}
