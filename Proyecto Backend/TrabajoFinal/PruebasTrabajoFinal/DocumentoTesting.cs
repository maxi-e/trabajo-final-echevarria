﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Controllers;
using Proyecto.Models.Response;
using Proyecto.Models;
using Proyecto.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Proyecto.Models.Request;
using Microsoft.AspNetCore.Http;

namespace PruebasTrabajoFinal
{
    public class DocumentoTesting
    {
        private readonly IDocumentoService _service;
        private readonly DocumentoController _controller;

        public DocumentoTesting()
        {
            _service = new DocumentoService();
            _controller = new DocumentoController(_service);
        }

        [Fact]
        public void GetDocumentos_OK()
        {
            var result = _controller.Get(4,2);

            Assert.IsType<OkObjectResult>(result);
        }


        [Fact]
        public void GetDocumentos_PacienteInexistente()
        {
            var result = (OkObjectResult)_controller.Get(50, 2); ;
            var respuesta = Assert.IsType<Respuesta>(result.Value);
            Assert.True(respuesta.Exito == 0);
        }

        [Fact]
        public void GetArchivo_OK()
        {
            var result = _controller.GetSoloArchivo(10);
            Assert.IsType<FileContentResult>(result);
        }
        [Fact]
        public void GetArchivo_PDF()
        {
            var result = (FileContentResult)_controller.GetSoloArchivo(10);
            Assert.True(result.ContentType == "application/pdf" );
        }
        [Fact]
        public void GetArchivo_PNG()
        {
            var result = (FileContentResult)_controller.GetSoloArchivo(1013);
            Assert.True(result.ContentType == "image/png");
        }
        [Fact]
        public void GetArchivo_JPG()
        {
            var result = (FileContentResult)_controller.GetSoloArchivo(1018);
            Assert.True(result.ContentType == "image/jpg");
        }
        [Fact]
        public void AddDocuento_Ok()
        {
            string path = "C:/Users/maxi-/Downloads/irregulares.pdf";
            IFormFile prueba;
            var stream = System.IO.File.OpenRead(path);
            using (var fs = stream)
            {
                prueba = new FormFile(fs, 0, fs.Length, "name", "name");
            }
            DocumentoRequest documento = new DocumentoRequest();
            documento.IdUsuario = 16;
            documento.IdTipoDocumento = 2;
            documento.FechaCreacion = DateTime.Now;
            documento.FechaSubida = DateTime.Now;
            documento.Formato = ".pdf";
            documento.Nombre = "test";
            documento.IdInstitucion = 1;
            documento.Fichero = prueba;

            var result = _controller.addDocumento(documento);
            
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public void AddDocuento_BadRequest_SinArchivo()
        {
            
            DocumentoRequest documento = new DocumentoRequest();
            documento.IdUsuario = 16;
            documento.IdTipoDocumento = 2;
            documento.FechaCreacion = DateTime.Now;
            documento.FechaSubida = DateTime.Now;
            documento.Formato = ".pdf";
            documento.Nombre = "test";
            documento.IdInstitucion = 1;
            var result = _controller.addDocumento(documento);

            Assert.IsType<BadRequestObjectResult>(result);
        }

    }
}
