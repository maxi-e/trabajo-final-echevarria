﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class TipoDocumento
    {
        public TipoDocumento()
        {
            Documentos = new HashSet<Documento>();
        }

        public int IdTipoDocumento { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<Documento> Documentos { get; set; }
    }
}
