﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Archivo
    {
        public Archivo()
        {
            Documentos = new HashSet<Documento>();
        }

        public int IdArchivo { get; set; }
        public string? Formato { get; set; }
        public byte[]? Fichero { get; set; }

        public virtual ICollection<Documento> Documentos { get; set; }
    }
}
