﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class ObraSocial
    {
        public ObraSocial()
        {
            Pacientes = new HashSet<Paciente>();
        }

        public int IdObraSocial { get; set; }
        public string? Descripcion { get; set; }
        public int? Estado { get; set; }

        public virtual ICollection<Paciente> Pacientes { get; set; }
    }
}
