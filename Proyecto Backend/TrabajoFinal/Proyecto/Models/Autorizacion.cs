﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Autorizacion
    {
        public int IdAutorizacion { get; set; }
        public int? IdUsuarioPaciente { get; set; }
        public int? IdUsuarioProfesional { get; set; }
        public int? Estado { get; set; }

        public virtual Usuario? IdUsuarioPacienteNavigation { get; set; }
        public virtual Usuario? IdUsuarioProfesionalNavigation { get; set; }
    }
}
