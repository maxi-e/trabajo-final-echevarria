﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Institucion
    {
        public Institucion()
        {
            Documentos = new HashSet<Documento>();
        }

        public int IdInstitucion { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<Documento> Documentos { get; set; }
    }
}
