﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Profesional
    {
        public int IdProfesional { get; set; }
        public int? IdUsuario { get; set; }
        public string? Especialidad { get; set; }
        public string? Matricula { get; set; }
        public int? Estado { get; set; }

        public virtual Usuario? IdUsuarioNavigation { get; set; }
    }
}
