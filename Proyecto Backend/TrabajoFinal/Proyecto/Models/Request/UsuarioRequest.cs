﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class UsuarioRequest
    {
        public int? IdUsuario { get; set; }
        public int? IdPersona { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public int? IdRol { get; set; }
        public string? token { get; set; }
        public int? Rol { get; set; }
        public ProfesionalRequest? Profesional  { get; set; }
        public PacienteRequest? Paciente { get; set; }

    }
}
