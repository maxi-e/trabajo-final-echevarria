﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class ProfesionalRequest
    {
        public int? IdProfesional { get; set; }
        public int? IdUsuario { get; set; }
        public string? Especialidad { get; set; }
        public string? Matricula { get; set; }
        public int? Estado { get; set; }
        public PersonaRequest? Persona { get; set; }
    }
}
