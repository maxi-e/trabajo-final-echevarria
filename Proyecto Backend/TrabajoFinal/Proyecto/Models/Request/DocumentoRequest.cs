﻿using Proyecto.Models.Request;
using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class DocumentoRequest 
    {
        public int? IdUsuario { get; set; }
        public int? IdTipoDocumento { get; set; }
        public string? Nombre { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaSubida { get; set; }
        public string? Formato { get; set; }
        public int? IdInstitucion { get; set; }
        public IFormFile? Fichero { get; set; }
    }
}
