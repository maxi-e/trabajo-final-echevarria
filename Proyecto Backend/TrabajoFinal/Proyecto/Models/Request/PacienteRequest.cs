﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class PacienteRequest
    {
        public int IdPaciente { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdObraSocial { get; set; }
        public string? NroAfiliado { get; set; }
        public int? Estado { get; set; }
        public PersonaRequest? Persona { get; set; }
    }
}
