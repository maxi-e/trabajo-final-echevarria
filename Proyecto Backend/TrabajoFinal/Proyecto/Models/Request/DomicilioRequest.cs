﻿namespace Proyecto.Models.Request
{
    public class DomicilioRequest
    {
        public string? Calle { get; set; }
        public string? Nro { get; set; }
        public string? Localidad { get; set; }
        public string? Provincia { get; set; }
    }
}
