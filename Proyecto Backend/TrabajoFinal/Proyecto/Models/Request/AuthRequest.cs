﻿using System.ComponentModel.DataAnnotations;

namespace Proyecto.Models.Request
{
    public class AuthRequest
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public int? IdRol { get; set; }
        public int IdPersona { get; set; }

    }
}
