﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class AutorizacionRequest
    {
        public int? IdUsuarioPaciente { get; set; }
        public int? IdUsuarioProfesional { get; set; }
    }
}
