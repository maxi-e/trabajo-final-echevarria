﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class InstitucionRequest
    {
        public int IdInstitucion { get; set; }
        public string? Descripcion { get; set; }
    }
}
