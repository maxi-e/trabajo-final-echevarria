﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class ArchivoRequest
    {
        public string? Formato { get; set; }
        public byte[]? Fichero { get; set; }
    }
}
