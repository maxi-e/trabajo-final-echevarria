﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Request
{
    public partial class TipoDocumentoRequest
    {
        public int IdTipoDocumento { get; set; }
        public string? Descripcion { get; set; }
    }
}
