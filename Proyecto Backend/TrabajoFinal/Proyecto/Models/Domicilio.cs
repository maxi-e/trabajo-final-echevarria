﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Domicilio
    {
        public Domicilio()
        {
            Personas = new HashSet<Persona>();
        }

        public int IdDomicilio { get; set; }
        public string? Calle { get; set; }
        public string? Nro { get; set; }
        public string? Localidad { get; set; }
        public string? Provincia { get; set; }

        public virtual ICollection<Persona> Personas { get; set; }
    }
}
