﻿using System;
using System.Collections.Generic;
    
namespace Proyecto.Models.Response
{
    public partial class InstitucionResponse
    {
        public int IdInstitucion { get; set; }
        public string? Descripcion { get; set; }
    }
}
