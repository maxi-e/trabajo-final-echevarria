﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class UsuarioResponse
    {
        public UsuarioResponse()
        {
            Pacientes = new HashSet<PacienteResponse>();
            Profesionals = new HashSet<ProfesionalResponse>();
        }

        public int IdUsuario { get; set; }
        public int? IdPersona { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public int? IdRol { get; set; }

        public virtual PersonaResponse? IdPersonaNavigation { get; set; }
        public virtual RolResponse? IdRolNavigation { get; set; }
        public virtual ICollection<PacienteResponse> Pacientes { get; set; }
        public virtual ICollection<ProfesionalResponse> Profesionals { get; set; }
    }
}
