﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class PacienteResponse
    {
        public int IdPaciente { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdObraSocial { get; set; }
        public string? NroAfiliado { get; set; }
        public int? Estado { get; set; }
        public PersonaResponse? Persona { get; set; }
    }
}
