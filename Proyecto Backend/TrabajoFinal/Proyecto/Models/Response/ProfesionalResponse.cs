﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class ProfesionalResponse
    {
        public int IdProfesional { get; set; }
        public int? IdUsuario { get; set; }
        public string? Especialidad { get; set; }
        public string? Matricula { get; set; }
        public int? Estado { get; set; }

        public PersonaResponse? Persona { get; set; }
    }
}
