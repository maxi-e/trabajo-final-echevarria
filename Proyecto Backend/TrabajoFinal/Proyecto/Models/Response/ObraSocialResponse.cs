﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class ObraSocialResponse
    {
        public int IdObraSocial { get; set; }
        public string? Descripcion { get; set; }

    }
}
