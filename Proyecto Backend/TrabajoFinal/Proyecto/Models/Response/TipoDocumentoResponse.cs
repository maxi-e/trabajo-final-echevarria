﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class TipoDocumentoResponse
    {
        public int IdTipoDocumento { get; set; }
        public string? Descripcion { get; set; }
    }
}
