﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class PersonaResponse
    {
        public int IdPersona { get; set; }
        public int? IdDomicilio { get; set; }
        public string? Dni { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido { get; set; }
        public int? Estado { get; set; }
        public string? Nacionalidad { get; set; }
        public string? Telefono { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string? Sexo { get; set; }
        public virtual DomicilioResponse? Domicilio { get; set; }
    }
}