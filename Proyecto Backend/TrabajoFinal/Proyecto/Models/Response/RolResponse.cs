﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class RolResponse
    {
        public RolResponse()
        {
            Usuarios = new HashSet<UsuarioResponse>();
        }

        public int IdRol { get; set; }
        public string? Descripcion { get; set; }

        public virtual ICollection<UsuarioResponse> Usuarios { get; set; }
    }
}
