﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class ArchivoResponse
    {
        public int IdArchivo { get; set; }
        public string? Formato { get; set; }
        public IFormFile? Fichero { get; set; }
    }
}
