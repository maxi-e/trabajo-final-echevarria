﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models.Response
{
    public partial class DomicilioResponse
    {
        public int IdDomicilio { get; set; }
        public string? Calle { get; set; }
        public string? Nro { get; set; }
        public string? Localidad { get; set; }
        public string? Provincia { get; set; }
    }
}
