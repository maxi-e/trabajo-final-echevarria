﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Proyecto.Models
{
    public partial class TrabajoFinalContext : DbContext
    {
        public TrabajoFinalContext()
        {
        }

        public TrabajoFinalContext(DbContextOptions<TrabajoFinalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Archivo> Archivos { get; set; } = null!;
        public virtual DbSet<Autorizacion> Autorizacions { get; set; } = null!;
        public virtual DbSet<Documento> Documentos { get; set; } = null!;
        public virtual DbSet<Domicilio> Domicilios { get; set; } = null!;
        public virtual DbSet<Institucion> Institucions { get; set; } = null!;
        public virtual DbSet<ObraSocial> ObraSocials { get; set; } = null!;
        public virtual DbSet<Paciente> Pacientes { get; set; } = null!;
        public virtual DbSet<Persona> Personas { get; set; } = null!;
        public virtual DbSet<Profesional> Profesionals { get; set; } = null!;
        public virtual DbSet<Rol> Rols { get; set; } = null!;
        public virtual DbSet<TipoDocumento> TipoDocumentos { get; set; } = null!;
        public virtual DbSet<Usuario> Usuarios { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=127.0.0.1,5434;Database=TrabajoFinal;Trusted_Connection=False;User ID=sa;Password=Passw0rd..;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Archivo>(entity =>
            {
                entity.HasKey(e => e.IdArchivo);

                entity.ToTable("Archivo");

                entity.Property(e => e.IdArchivo).HasColumnName("idArchivo");

                entity.Property(e => e.Fichero)
                    .HasColumnType("image")
                    .HasColumnName("fichero");

                entity.Property(e => e.Formato)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .HasColumnName("formato");
            });

            modelBuilder.Entity<Autorizacion>(entity =>
            {
                entity.HasKey(e => e.IdAutorizacion);

                entity.ToTable("Autorizacion");

                entity.Property(e => e.IdAutorizacion).HasColumnName("idAutorizacion");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.IdUsuarioPaciente).HasColumnName("idUsuarioPaciente");

                entity.Property(e => e.IdUsuarioProfesional).HasColumnName("idUsuarioProfesional");

                entity.HasOne(d => d.IdUsuarioPacienteNavigation)
                    .WithMany(p => p.AutorizacionIdUsuarioPacienteNavigations)
                    .HasForeignKey(d => d.IdUsuarioPaciente)
                    .HasConstraintName("FK_Autorizacion_Usuario");

                entity.HasOne(d => d.IdUsuarioProfesionalNavigation)
                    .WithMany(p => p.AutorizacionIdUsuarioProfesionalNavigations)
                    .HasForeignKey(d => d.IdUsuarioProfesional)
                    .HasConstraintName("FK_Autorizacion_Usuario1");
            });

            modelBuilder.Entity<Documento>(entity =>
            {
                entity.HasKey(e => e.IdDocumento)
                    .HasName("PK_Documento_1");

                entity.ToTable("Documento");

                entity.Property(e => e.IdDocumento).HasColumnName("idDocumento");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaCreacion");

                entity.Property(e => e.FechaSubida)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaSubida");

                entity.Property(e => e.Formato)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("formato");

                entity.Property(e => e.IdArchivo).HasColumnName("idArchivo");

                entity.Property(e => e.IdInstitucion).HasColumnName("idInstitucion");

                entity.Property(e => e.IdTipoDocumento).HasColumnName("idTipoDocumento");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("nombre");

                entity.HasOne(d => d.IdArchivoNavigation)
                    .WithMany(p => p.Documentos)
                    .HasForeignKey(d => d.IdArchivo)
                    .HasConstraintName("FK_Documento_Archivo");

                entity.HasOne(d => d.IdInstitucionNavigation)
                    .WithMany(p => p.Documentos)
                    .HasForeignKey(d => d.IdInstitucion)
                    .HasConstraintName("FK_Documento_Institucion");

                entity.HasOne(d => d.IdTipoDocumentoNavigation)
                    .WithMany(p => p.Documentos)
                    .HasForeignKey(d => d.IdTipoDocumento)
                    .HasConstraintName("FK_Documento_TipoDocumento");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Documentos)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_Documento_Usuario");
            });

            modelBuilder.Entity<Domicilio>(entity =>
            {
                entity.HasKey(e => e.IdDomicilio);

                entity.ToTable("Domicilio");

                entity.Property(e => e.IdDomicilio).HasColumnName("idDomicilio");

                entity.Property(e => e.Calle)
                    .HasMaxLength(50)
                    .HasColumnName("calle");

                entity.Property(e => e.Localidad)
                    .HasMaxLength(50)
                    .HasColumnName("localidad");

                entity.Property(e => e.Nro)
                    .HasMaxLength(50)
                    .HasColumnName("nro");

                entity.Property(e => e.Provincia)
                    .HasMaxLength(50)
                    .HasColumnName("provincia");
            });

            modelBuilder.Entity<Institucion>(entity =>
            {
                entity.HasKey(e => e.IdInstitucion);

                entity.ToTable("Institucion");

                entity.Property(e => e.IdInstitucion).HasColumnName("idInstitucion");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");
            });

            modelBuilder.Entity<ObraSocial>(entity =>
            {
                entity.HasKey(e => e.IdObraSocial);

                entity.ToTable("ObraSocial");

                entity.Property(e => e.IdObraSocial).HasColumnName("idObraSocial");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .HasColumnName("descripcion");

                entity.Property(e => e.Estado).HasColumnName("estado");
            });

            modelBuilder.Entity<Paciente>(entity =>
            {
                entity.HasKey(e => e.IdPaciente);

                entity.ToTable("Paciente");

                entity.Property(e => e.IdPaciente).HasColumnName("idPaciente");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.IdObraSocial).HasColumnName("idObraSocial");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.NroAfiliado)
                    .HasMaxLength(50)
                    .HasColumnName("nroAfiliado");

                entity.HasOne(d => d.IdObraSocialNavigation)
                    .WithMany(p => p.Pacientes)
                    .HasForeignKey(d => d.IdObraSocial)
                    .HasConstraintName("FK_Paciente_ObraSocial");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Pacientes)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_Paciente_Usuario");
            });

            modelBuilder.Entity<Persona>(entity =>
            {
                entity.HasKey(e => e.IdPersona);

                entity.ToTable("Persona");

                entity.Property(e => e.IdPersona).HasColumnName("idPersona");

                entity.Property(e => e.Apellido)
                    .HasMaxLength(50)
                    .HasColumnName("apellido");

                entity.Property(e => e.Dni)
                    .HasMaxLength(8)
                    .HasColumnName("dni");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.FechaNacimiento)
                    .HasColumnType("datetime")
                    .HasColumnName("fechaNacimiento");

                entity.Property(e => e.IdDomicilio).HasColumnName("idDomicilio");

                entity.Property(e => e.Nacionalidad)
                    .HasMaxLength(50)
                    .HasColumnName("nacionalidad");

                entity.Property(e => e.Nombre)
                    .HasMaxLength(50)
                    .HasColumnName("nombre");

                entity.Property(e => e.Sexo)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("sexo");

                entity.Property(e => e.Telefono)
                    .HasMaxLength(15)
                    .HasColumnName("telefono");

                entity.HasOne(d => d.IdDomicilioNavigation)
                    .WithMany(p => p.Personas)
                    .HasForeignKey(d => d.IdDomicilio)
                    .HasConstraintName("FK_Persona_Domicilio");
            });

            modelBuilder.Entity<Profesional>(entity =>
            {
                entity.HasKey(e => e.IdProfesional);

                entity.ToTable("Profesional");

                entity.Property(e => e.IdProfesional).HasColumnName("idProfesional");

                entity.Property(e => e.Especialidad)
                    .HasMaxLength(50)
                    .HasColumnName("especialidad");

                entity.Property(e => e.Estado).HasColumnName("estado");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Matricula)
                    .HasMaxLength(50)
                    .HasColumnName("matricula");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Profesionals)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_Profesional_Usuario");
            });

            modelBuilder.Entity<Rol>(entity =>
            {
                entity.HasKey(e => e.IdRol);

                entity.ToTable("Rol");

                entity.Property(e => e.IdRol).HasColumnName("idRol");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .HasColumnName("descripcion");
            });

            modelBuilder.Entity<TipoDocumento>(entity =>
            {
                entity.HasKey(e => e.IdTipoDocumento);

                entity.ToTable("TipoDocumento");

                entity.Property(e => e.IdTipoDocumento).HasColumnName("idTipoDocumento");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("descripcion");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.ToTable("Usuario");

                entity.Property(e => e.IdUsuario).HasColumnName("idUsuario");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.IdPersona).HasColumnName("idPersona");

                entity.Property(e => e.IdRol).HasColumnName("idRol");

                entity.Property(e => e.Password)
                    .HasMaxLength(256)
                    .HasColumnName("password");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.IdPersona)
                    .HasConstraintName("FK_Usuario_Persona");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.IdRol)
                    .HasConstraintName("FK_Usuario_Rol");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
