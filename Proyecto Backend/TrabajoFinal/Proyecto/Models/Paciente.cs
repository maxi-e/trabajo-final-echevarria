﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Paciente
    {
        public int IdPaciente { get; set; }
        public int? IdUsuario { get; set; }
        public int? IdObraSocial { get; set; }
        public string? NroAfiliado { get; set; }
        public int? Estado { get; set; }

        public virtual ObraSocial? IdObraSocialNavigation { get; set; }
        public virtual Usuario? IdUsuarioNavigation { get; set; }
    }
}
