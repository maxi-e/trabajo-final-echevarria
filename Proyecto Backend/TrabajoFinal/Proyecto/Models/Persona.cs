﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Persona
    {
        public Persona()
        {
            Usuarios = new HashSet<Usuario>();
        }

        public int IdPersona { get; set; }
        public int? IdDomicilio { get; set; }
        public string? Dni { get; set; }
        public string? Nombre { get; set; }
        public string? Apellido { get; set; }
        public int? Estado { get; set; }
        public string? Nacionalidad { get; set; }
        public string? Telefono { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string? Sexo { get; set; }

        public virtual Domicilio? IdDomicilioNavigation { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}
