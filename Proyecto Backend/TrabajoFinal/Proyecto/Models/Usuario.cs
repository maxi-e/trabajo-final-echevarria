﻿using System;
using System.Collections.Generic;

namespace Proyecto.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            AutorizacionIdUsuarioPacienteNavigations = new HashSet<Autorizacion>();
            AutorizacionIdUsuarioProfesionalNavigations = new HashSet<Autorizacion>();
            Documentos = new HashSet<Documento>();
            Pacientes = new HashSet<Paciente>();
            Profesionals = new HashSet<Profesional>();
        }

        public int IdUsuario { get; set; }
        public int? IdPersona { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public int? IdRol { get; set; }

        public virtual Persona? IdPersonaNavigation { get; set; }
        public virtual Rol? IdRolNavigation { get; set; }
        public virtual ICollection<Autorizacion> AutorizacionIdUsuarioPacienteNavigations { get; set; }
        public virtual ICollection<Autorizacion> AutorizacionIdUsuarioProfesionalNavigations { get; set; }
        public virtual ICollection<Documento> Documentos { get; set; }
        public virtual ICollection<Paciente> Pacientes { get; set; }
        public virtual ICollection<Profesional> Profesionals { get; set; }
    }
}
