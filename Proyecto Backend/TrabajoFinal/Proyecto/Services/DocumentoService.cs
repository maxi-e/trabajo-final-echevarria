﻿using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Models;
using Proyecto.Tools;
using Microsoft.Data.SqlClient;
using System.Data;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Collections;
using Microsoft.AspNetCore.Mvc;
using AutoMapper.Execution;

namespace Proyecto.Services
{
    public class DocumentoService:IDocumentoService
    {
        public async Task<Respuesta> GuardarDocumento(DocumentoRequest documentoRequest) {

            Respuesta oRespuesta = new Respuesta();
            try
            {
                byte[] fileBytes;
                if (documentoRequest.Fichero != null && (documentoRequest.Fichero.Length > 0))
                {
                    using (var ms = new MemoryStream())
                    {
                        await documentoRequest.Fichero.CopyToAsync(ms);
                        fileBytes = ms.ToArray();
                    }

                    FileInfo fi = new FileInfo(documentoRequest.Fichero.FileName);

                    using (TrabajoFinalContext db = new TrabajoFinalContext())
                    {
                        using (var transaction = db.Database.BeginTransaction())
                        {
                            //guardado del archivo
                            Archivo archivo = new Archivo();
                            archivo.Fichero = fileBytes;
                            archivo.Formato = fi.Extension;
                            db.Archivos.Add(archivo);
                            db.SaveChanges();

                            //guardado del documento
                            Documento documento = new Documento();

                            documento.Nombre = documentoRequest.Nombre;//Path.GetFileNameWithoutExtension(documento.Nombre);
                            documento.FechaCreacion = DateTime.Today; // verificar si funciona
                            documento.FechaSubida = DateTime.Today;
                            documento.IdUsuario = documentoRequest.IdUsuario;
                            documento.IdTipoDocumento = documentoRequest.IdTipoDocumento;
                            documento.IdInstitucion = documentoRequest.IdInstitucion;
                            documento.Formato = fi.Extension;
                            documento.IdArchivo = db.Archivos.ToList().LastOrDefault().IdArchivo;
                            db.Documentos.Add(documento);
                            db.SaveChanges();
                            transaction.Commit();
                        }
                    }
                    oRespuesta.Exito = 1;
                    oRespuesta.Mensaje = "El docuento se guardo correctamente";
                    return oRespuesta;
                }
                else {
                    oRespuesta.Exito = 0;
                    oRespuesta.Mensaje = "Ocurrio un error al cargar el documento";
                    return oRespuesta;
                }
            }
            catch (Exception ex) {
                oRespuesta.Exito = 0;
                oRespuesta.Mensaje = "Ocurrio un error al cargar el documento, reintente mas tarde";
                return oRespuesta;

            }
        }

        public Respuesta getDocumentos(int idUsuario, int idTipoDocumento) {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var lst = db.Documentos
                                        .Include(x=>x.IdArchivoNavigation)
                                        .Where(x=>x.IdUsuario == idUsuario && x.IdTipoDocumento == idTipoDocumento)
                                        .ToList();

                    MapperConfiguration config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Documento, DocumentoResponse>();
                    });

                    var mapper = new Mapper(config);
                    List<DocumentoResponse> documentoRespose = mapper.Map<List<DocumentoResponse>>(lst);
                    if (documentoRespose.Count > 0)
                    {
                        oRespuesta.Data = documentoRespose;
                        oRespuesta.Exito = 1;
                    }
                    else {
                        oRespuesta.Mensaje = "Usuario inexistente";
                        oRespuesta.Exito = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
                oRespuesta.Exito = 0;
            }
            return oRespuesta;
        }

        public Respuesta getArchivo(int idArchivo){

            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    ArchivoResponse archivoResponse = new ArchivoResponse();
                    var archivo = db.Archivos
                                        .Where(x => x.IdArchivo == idArchivo)
                                        .FirstOrDefault();
                    var documento = db.Documentos
                                        .Where(x => x.IdArchivo == idArchivo)
                                        .FirstOrDefault();

                    using (var ms = new MemoryStream(archivo.Fichero))
                    {
                        IFormFile fromFile = new FormFile(ms, 0, ms.Length,
                            documento.Nombre+"A",
                            documento.Nombre+"A" + documento.Formato
                        )
                        {
                            Headers = new HeaderDictionary(),
                            ContentType = "application/pdf"
                        };
                        archivoResponse.Fichero = fromFile;
                      
                    }
                    archivoResponse.IdArchivo = idArchivo;
                    archivoResponse.Formato = archivo.Formato;
                    oRespuesta.Data = archivoResponse;
                    oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Exito = 0;
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }

        public FileContentResult getSoloArchivo(int idArchivo)
        {
            Respuesta oRespuesta = new Respuesta();
            
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                string tipo = "";
                ArchivoResponse archivoResponse = new ArchivoResponse();
                var archivo = db.Archivos
                                        .Where(x => x.IdArchivo == idArchivo)
                                        .FirstOrDefault();
                
                    return new FileContentResult(archivo.Fichero, tipoContenido(archivo.Formato));
                
            }
        }

        private string tipoContenido(string tipo) {
            switch (tipo)
            {
                case ".pdf":
                    return "application/pdf";
                case ".png":
                    return "image/png";
                case ".jpeg":
                    return "image/jpeg";
                case ".jpg":
                    return "image/jpg";
                default:
                    return "text/plain";
            }
        }

    }
     
}
