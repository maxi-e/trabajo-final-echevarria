﻿using Proyecto.Models.Request;
using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public interface IProfesionalServices
    {
        Respuesta GetProfesional();
        Respuesta GetProfesionalId(int idProfesional);
        void Edit(ProfesionalRequest oModelProfesional);
    }
}
