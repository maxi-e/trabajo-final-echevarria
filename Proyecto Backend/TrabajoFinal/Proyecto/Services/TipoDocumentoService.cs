﻿using AutoMapper;
using Proyecto.Models.Response;
using Proyecto.Models;

namespace Proyecto.Services
{
    public class TipoDocumentoService:ITipoDocumentoService
    {

        public Respuesta getTipoDocumento()
        {

            Respuesta oRespuesta = new Respuesta();
            try
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<TipoDocumento, TipoDocumentoResponse>()
                );

                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    List<TipoDocumento> lst = db.TipoDocumentos.ToList();

                    var mapper = new Mapper(config);
                    List<TipoDocumentoResponse> institucionesRespose = mapper.Map<List<TipoDocumentoResponse>>(lst);

                    oRespuesta.Data = institucionesRespose;
                    oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }
    }

}
