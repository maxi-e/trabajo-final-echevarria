﻿using AutoMapper;
using Proyecto.Models;
using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public class ObraSocialService : IObraSocialService
    {
        public Respuesta GetObraSocial()
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<ObraSocial, ObraSocialResponse>()
                );

                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    List<ObraSocial> lst = db.ObraSocials.Where(x => x.Estado == 1).OrderByDescending(x => x.IdObraSocial).ToList();

                    var mapper = new Mapper(config);
                    List<ObraSocialResponse> obrasSocialesRespose = mapper.Map<List<ObraSocialResponse>>(lst);

                    oRespuesta.Data = obrasSocialesRespose;
                    oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }

        public Respuesta GetObraSocialId(int idObraSocial)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var lst = db.ObraSocials.Where(x => x.IdObraSocial == idObraSocial).ToList();//.Find(idProfesional);
                    oRespuesta.Exito = 1;
                    oRespuesta.Data = lst;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }
    }
}
