﻿using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public interface IInstitucionService
    {
        Respuesta GetInstitucion();
    }
}
