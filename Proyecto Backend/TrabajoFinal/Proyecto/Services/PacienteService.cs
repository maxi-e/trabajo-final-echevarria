﻿using Proyecto.Models;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Microsoft.EntityFrameworkCore;


using AutoMapper;


namespace Proyecto.Services
{
    public class PacienteService : IPacienteService
    {
        public void Edit(PacienteRequest oModelPaciente)
        {
            using (TrabajoFinalContext db = new TrabajoFinalContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Modifica los datos del profesional   
                        Paciente oPaciente = db.Pacientes.Where(x => x.IdUsuario == oModelPaciente.IdUsuario).First();

                        // oProfesional.IdUsuario = db.Usuarios.ToList().LastOrDefault().IdUsuario;
                        oPaciente.NroAfiliado = oModelPaciente.NroAfiliado;
                        oPaciente.IdObraSocial = oModelPaciente.IdObraSocial;
                        //oPaciente.Estado = oModelPaciente.Estado;

                        Usuario oUsuario = db.Usuarios.Where(x => x.IdUsuario == oModelPaciente.IdUsuario).FirstOrDefault();

                        db.Entry(oPaciente).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        db.SaveChanges();

                        //Modifica los datos de la persona
                        if (oModelPaciente.Persona != null)
                        {
                            Models.Persona oPersona = db.Personas.Where(x => x.IdPersona == oUsuario.IdPersona).FirstOrDefault();
                            oPersona.Dni = oModelPaciente.Persona.Dni;
                            oPersona.Apellido = oModelPaciente.Persona.Apellido;
                            oPersona.Nombre = oModelPaciente.Persona.Nombre;
                            oPersona.Nacionalidad = oModelPaciente.Persona.Nacionalidad;
                            oPersona.Telefono = oModelPaciente.Persona.Telefono;
                            oPersona.Estado = oModelPaciente.Persona.Estado;
                            oPersona.FechaNacimiento = oModelPaciente.Persona.FechaNacimiento;
                            oPersona.Sexo = oModelPaciente.Persona.Sexo;

                            db.Entry(oPersona).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                            db.SaveChanges();

                            if (oModelPaciente.Persona.Domicilio != null)
                            {
                                ////Modifica los datos del domicilio
                                Models.Domicilio oDomicilio = db.Domicilios.Where(x => x.IdDomicilio == oPersona.IdDomicilio).FirstOrDefault();
                                oDomicilio.Nro = oModelPaciente.Persona.Domicilio.Nro;
                                oDomicilio.Calle = oModelPaciente.Persona.Domicilio.Calle;
                                oDomicilio.Localidad = oModelPaciente.Persona.Domicilio.Localidad;
                                oDomicilio.Provincia = oModelPaciente.Persona.Domicilio.Provincia;
                                db.Entry(oDomicilio).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw new Exception("Ocurrio un error al editar los datos");
                    }
                }
            }
        }

        public Respuesta AddAutorizacion(AutorizacionRequest autorizacionRequest){

            Respuesta oRespuesta = new Respuesta();
            Autorizacion oAutorizacion = new Autorizacion();
            using (TrabajoFinalContext db = new TrabajoFinalContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        oAutorizacion = db.Autorizacions.Where(x => x.IdUsuarioProfesional == autorizacionRequest.IdUsuarioProfesional && x.IdUsuarioPaciente == autorizacionRequest.IdUsuarioPaciente).FirstOrDefault();
                        if (oAutorizacion == null)
                        {
                            oAutorizacion = new Autorizacion();
                            oAutorizacion.IdUsuarioProfesional = autorizacionRequest.IdUsuarioProfesional;
                            oAutorizacion.IdUsuarioPaciente = autorizacionRequest.IdUsuarioPaciente;
                            oAutorizacion.Estado = 1;
                            db.Autorizacions.Add(oAutorizacion);
                            db.SaveChanges();
                            transaction.Commit();
                            oRespuesta.Mensaje = "Se creo el permiso exitosamente";
                        }
                        else
                        {
                            if (oAutorizacion.Estado == 0)
                            {
                                oAutorizacion.Estado = 1;
                                db.Entry(oAutorizacion).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                                db.SaveChanges();
                                transaction.Commit();
                                oRespuesta.Mensaje = "Se actualizo el permiso exitosamente";
                            }
                        }
                        oRespuesta.Exito = 1;

                        return oRespuesta;

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        oRespuesta.Mensaje = "Ocurrio un error, reintentalo";
                        oRespuesta.Exito = 0;
                        return oRespuesta;

                    }
                }
            }

        }

        public Respuesta GetPacientes(int idUsuarioProfesional)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var autorizados = db.Autorizacions
                                                    .Where(x=>x.IdUsuarioProfesional==idUsuarioProfesional)
                                                    .Select(x => x.IdUsuarioPaciente)
                                                    .ToList();
                    var lst = db.Pacientes
                                        .Include(x => x.IdUsuarioNavigation)
                                        .ThenInclude(x=>x.IdPersonaNavigation)
                                        .ThenInclude(x=>x.IdDomicilioNavigation)
                                        .Include(x=>x.IdObraSocialNavigation)
                                        .Where(x=> autorizados.Contains(x.IdUsuario))
                                        .ToList();//.Where(x => x.Estado == 1).OrderByDescending(x => x.IdPaciente).ToList();

                    MapperConfiguration config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Domicilio, DomicilioResponse>();
                        cfg.CreateMap<Persona, PersonaResponse>();
                        cfg.CreateMap<Paciente, PacienteResponse>()
                        .ForMember(dest => dest.Persona, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation))
                        .ForPath(dest => dest.Persona.Domicilio, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation.IdDomicilioNavigation));
                    });


                    var mapper = new Mapper(config);
                    List<PacienteResponse> pacientesRespose = mapper.Map<List<PacienteResponse>>(lst);

                    oRespuesta.Data = pacientesRespose;
                oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }

        public Respuesta GetProfesionalesAutorizados(int idUsuarioPaciente) {

            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var autorizados = db.Autorizacions
                                                    .Where(x => x.IdUsuarioPaciente == idUsuarioPaciente && x.Estado == 1)
                                                    .Select(x => x.IdUsuarioProfesional)
                                                    .ToList();
                    var lst = db.Profesionals
                                        .Include(x => x.IdUsuarioNavigation)
                                        .ThenInclude(x => x.IdPersonaNavigation)
                                        .ThenInclude(x => x.IdDomicilioNavigation)
                                        .Where(x => autorizados.Contains(x.IdUsuario))
                                        .ToList();

                    MapperConfiguration config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Domicilio, DomicilioResponse>();
                        cfg.CreateMap<Persona, PersonaResponse>();
                        cfg.CreateMap<Profesional, ProfesionalResponse>()
                        .ForMember(dest => dest.Persona, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation))
                        .ForPath(dest => dest.Persona.Domicilio, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation.IdDomicilioNavigation));
                    });


                    var mapper = new Mapper(config);
                    List<ProfesionalResponse> profesionalesRespose = mapper.Map<List<ProfesionalResponse>>(lst);

                    oRespuesta.Data = profesionalesRespose;
                    oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }

        public Respuesta GetPacienteId(int idUsuario)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var paciente = db.Pacientes
                                        .Include(x => x.IdUsuarioNavigation)
                                        .ThenInclude(x => x.IdPersonaNavigation)
                                        .ThenInclude(x => x.IdDomicilioNavigation)
                                        .Include(x => x.IdObraSocialNavigation)
                                        .Where(x => x.IdUsuario == idUsuario).FirstOrDefault();//.Find(idProfesional);

                    MapperConfiguration config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Domicilio, DomicilioResponse>();
                        cfg.CreateMap<Persona, PersonaResponse>();
                        cfg.CreateMap<Paciente, PacienteResponse>()
                        .ForMember(dest => dest.Persona, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation))
                        .ForPath(dest => dest.Persona.Domicilio, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation.IdDomicilioNavigation));
                    });

                    var mapper = new Mapper(config);
                    PacienteResponse pacientesRespose = mapper.Map<PacienteResponse>(paciente);

                    oRespuesta.Exito = 1;
                    oRespuesta.Data = pacientesRespose;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }

        public Respuesta removerAutorizacion(AutorizacionRequest autorizacionRequest) {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    Autorizacion oAutorizacion = new Autorizacion();
                    oAutorizacion = db.Autorizacions
                                                    .Where(x => x.IdUsuarioProfesional == autorizacionRequest.IdUsuarioProfesional && 
                                                    x.IdUsuarioPaciente == autorizacionRequest.IdUsuarioPaciente)
                                                    .First();
                    oAutorizacion.Estado = 0;
                    db.Entry(oAutorizacion).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    db.SaveChanges();
                    oRespuesta.Mensaje = "Se quitaron los permisos otorgados";
                    oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }




    }
}
