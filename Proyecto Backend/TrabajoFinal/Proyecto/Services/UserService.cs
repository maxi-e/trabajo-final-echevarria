﻿using Microsoft.Extensions.Options;
using Proyecto.Models.Common;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Models;
using Proyecto.Tools;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;

namespace Proyecto.Services
{
    public class UserService : IUserService
    {

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {

            _appSettings = appSettings.Value;
        }

        public UserResponse Auth(AuthRequest model)
        {
            UserResponse userresponse = new UserResponse();
            using (var db = new TrabajoFinalContext())
            {
                try
                {
                    string spassword = Encrypt.GetSHA256(model.Password);
                    var usuario = db.Usuarios.Where(d => d.Email == model.Email && d.Password == spassword).FirstOrDefault();
                    Rol rol = db.Rols.Where(d => d.IdRol == usuario.IdRol).FirstOrDefault();
                    usuario.IdRolNavigation = rol;
                    if (usuario == null) return null;

                    userresponse.Email = usuario.Email;
                    userresponse.Token = GetToken(usuario);
                    userresponse.UserId = usuario.IdUsuario;
                    userresponse.Rol = rol.IdRol;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return userresponse;
        }

        //Registro de un nuevo usuario, adaptar
        public Respuesta RegistrarMedico(UsuarioRequest oModel)
        {
            Respuesta oRespuesta = new Respuesta();
            int idUsuario;
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                var usuario = db.Usuarios.Where(d => d.Email == oModel.Email).FirstOrDefault();
                if ( usuario != null){
                    oRespuesta.Exito = 0;
                    oRespuesta.Mensaje = "El mail ya se encuentra registrado";
                    return oRespuesta;
                }

                    using (var transaction = db.Database.BeginTransaction())
                    {
                        try
                        { 
                        //Guarda el domicilio    
                        Domicilio oDomicilio = new Domicilio();  
                        oDomicilio.Nro = oModel.Profesional.Persona.Domicilio.Nro;
                        oDomicilio.Calle = oModel.Profesional.Persona.Domicilio.Calle;
                        oDomicilio.Localidad = oModel.Profesional.Persona.Domicilio.Localidad;
                        oDomicilio.Provincia = oModel.Profesional.Persona.Domicilio.Provincia;
                        db.Domicilios.Add(oDomicilio);
                        db.SaveChanges();

                        //Guarda los datos de la persona
                        Persona oPersona = new Persona();   
                        oPersona.Dni = oModel.Profesional.Persona.Dni;
                        oPersona.Apellido = oModel.Profesional.Persona.Apellido;
                        oPersona.Nombre = oModel.Profesional.Persona.Nombre;
                        oPersona.FechaNacimiento = oModel.Profesional.Persona.FechaNacimiento;
                        oPersona.Sexo = oModel.Profesional.Persona.Sexo; 
                        oPersona.IdDomicilio = db.Domicilios.ToList().LastOrDefault().IdDomicilio;
                        oPersona.Nacionalidad = oModel.Profesional.Persona.Nacionalidad;
                        oPersona.Telefono = oModel.Profesional.Persona.Telefono;
                        oPersona.Estado = 1;
                        db.Personas.Add(oPersona);
                        db.SaveChanges();

                        //Guarda los datos del usuario
                        Usuario oUsuario = new Usuario();
                        oUsuario.IdPersona = db.Personas.ToList().LastOrDefault().IdPersona;
                        oUsuario.Email = oModel.Email;
                        oUsuario.Password = Encrypt.GetSHA256(oModel.Password);
                        oUsuario.IdRol = 1;
                        db.Usuarios.Add(oUsuario);
                        db.SaveChanges();

                        //Guarda los datos del profesional
                        Profesional oProfesional = new Profesional();
                        oProfesional.IdUsuario = db.Usuarios.ToList().LastOrDefault().IdUsuario;
                        oProfesional.Matricula = oModel.Profesional.Matricula;
                        oProfesional.Especialidad = oModel.Profesional.Especialidad;
                        oProfesional.Estado = 1;
                        db.Profesionals.Add(oProfesional);
                        db.SaveChanges();


                        transaction.Commit(); 
                        oRespuesta.Exito = 1;
                        oRespuesta.Mensaje = "Se modifico correctamente";
                        return oRespuesta;
                        }
                        catch (Exception ex)
                        {
                            oRespuesta.Exito = 0;
                            oRespuesta.Mensaje = ex.Message;
                            return oRespuesta;
                        }
                    }
                }
            }

        public Respuesta RegistrarPaciente(UsuarioRequest oModel)
        {
            Respuesta oRespuesta = new Respuesta();

            using (TrabajoFinalContext db = new TrabajoFinalContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Guarda el domicilio    
                        Domicilio oDomicilio = new Domicilio();
                        oDomicilio.Nro = oModel.Paciente.Persona.Domicilio.Nro;
                        oDomicilio.Calle = oModel.Paciente.Persona.Domicilio.Calle;
                        oDomicilio.Localidad = oModel.Paciente.Persona.Domicilio.Localidad;
                        oDomicilio.Provincia = oModel.Paciente.Persona.Domicilio.Provincia;
                        db.Domicilios.Add(oDomicilio);
                        db.SaveChanges();

                        //Guarda los datos de la persona
                        Persona oPersona = new Persona();
                        oPersona.Dni = oModel.Paciente.Persona.Dni;
                        oPersona.Apellido = oModel.Paciente.Persona.Apellido;
                        oPersona.Nombre = oModel.Paciente.Persona.Nombre;
                        oPersona.FechaNacimiento = oModel.Paciente.Persona.FechaNacimiento;
                        oPersona.Sexo = oModel.Paciente.Persona.Sexo; 
                        oPersona.IdDomicilio = db.Domicilios.ToList().LastOrDefault().IdDomicilio;
                        oPersona.Nacionalidad = oModel.Paciente.Persona.Nacionalidad;
                        oPersona.Telefono = oModel.Paciente.Persona.Telefono;
                        oPersona.Estado = 1;
                        db.Personas.Add(oPersona);
                        db.SaveChanges();

                        //Guarda los datos del usuario
                        Usuario oUsuario = new Usuario();
                        oUsuario.IdPersona = db.Personas.ToList().LastOrDefault().IdPersona;
                        oUsuario.Email = oModel.Email;
                        oUsuario.Password = Encrypt.GetSHA256(this.GeneraContrasenia(oModel.Paciente.Persona));
                        oUsuario.IdRol = 2;
                        db.Usuarios.Add(oUsuario);
                        db.SaveChanges();

                        //Guarda los datos del profesional
                        Paciente oPaciente = new Paciente();
                        oPaciente.IdUsuario = db.Usuarios.ToList().LastOrDefault().IdUsuario;
                        oPaciente.NroAfiliado = oModel.Paciente.NroAfiliado;
                        oPaciente.IdObraSocial = oModel.Paciente.IdObraSocial;
                        oPaciente.Estado = 1;
                        db.Pacientes.Add(oPaciente);
                        db.SaveChanges();


                        transaction.Commit();
                        oRespuesta.Exito = 1;
                        oRespuesta.Mensaje = "Se Registro Paciente correctamente";
                        return oRespuesta;
                    }
                    catch (Exception ex)
                    {
                        oRespuesta.Exito = 0;
                        oRespuesta.Mensaje = ex.Message;
                        return oRespuesta;
                    }
                }
            }
        }
        public string GeneraContrasenia(PersonaRequest? oPersona)
        {

            return oPersona.Apellido + oPersona.Dni;
        }
        private string GetToken(Usuario usuario)
        {

            var tokenHandler = new JwtSecurityTokenHandler();
            var llave = Encoding.ASCII.GetBytes(_appSettings.Secreto);
            var tokenDescritor = new SecurityTokenDescriptor
            {

                Subject = new ClaimsIdentity(
                    new Claim[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, usuario.IdUsuario.ToString()),
                        new Claim(ClaimTypes.Email, usuario.Email),
                        new Claim(ClaimTypes.Role, usuario.IdRolNavigation.Descripcion)
                    }
                    ),
                Expires = DateTime.UtcNow.AddDays(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(llave), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescritor);
            return tokenHandler.WriteToken(token);
        }

        private string GetTokenPassword(Usuario usuario)
        {

            var tokenHandler = new JwtSecurityTokenHandler();
            var llave = Encoding.ASCII.GetBytes(_appSettings.Secreto);
            var tokenDescritor = new SecurityTokenDescriptor
            {

                Subject = new ClaimsIdentity(
                    new Claim[]
                    {
                        new Claim(ClaimTypes.NameIdentifier, usuario.IdUsuario.ToString()),
                        new Claim(ClaimTypes.Email, usuario.Email),
                    }
                    ),
                Expires = DateTime.UtcNow.AddDays(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(llave), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescritor);
            return tokenHandler.WriteToken(token);
        }

        public string validarCorreo(string email)
        {
            using (var db = new TrabajoFinalContext())
            {
                try
                {
                    string token = "";
                    //UserResponse userresponse = new UserResponse();
                    Usuario usuario = db.Usuarios.Where(d => d.Email == email).FirstOrDefault();
                    if (usuario != null)
                    {
                        token = GetTokenPassword(usuario);
                        //userresponse.UserId = usuario.IdUsuario;
                        enviarMail(token, email);
                        return token;
                    }
                    else return null;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        private void enviarMail(string token, string email)
        {
            string EmailOrigen = "maxijonatan21@gmail.com"; //gmail, configurar la cuenta
            string EmailDestino = "maxi-e@hotmail.com.ar"; //email;
            string contraseña = "wihtjaymevqumgwu";

            string boton = $"<a href=\"http://localhost:4200/recuperarcontraseña/{token}\" style =\"border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; background-color: #008CBA;\" target =\"_blank\" >Recuperar Contraseña</a>";
            //el link apunta al front, habria que modificarlo segun donde se despliega
            string mailBody = $"Para recuperar tu contraseña haz click en el boton: <hr>" + boton;
            ;

            MailMessage oMailMessage = new MailMessage(EmailOrigen, EmailDestino, "Recuperacion de contraseña", mailBody);

            oMailMessage.IsBodyHtml = true;

            SmtpClient oSmtpClient = new SmtpClient("smtp.gmail.com");
            oSmtpClient.EnableSsl = true;
            oSmtpClient.UseDefaultCredentials = false;
            //oSmtpClient.Host = "smtp.gmail.com";
            oSmtpClient.Port = 587;
            oSmtpClient.Credentials = new System.Net.NetworkCredential(EmailOrigen, contraseña);
            oSmtpClient.Send(oMailMessage);
            oSmtpClient.Dispose();
        }

        public Respuesta modificarContraseña(AuthRequest model)
        {

            Respuesta oRespuesta = new Respuesta();


            using (TrabajoFinalContext db = new TrabajoFinalContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Usuario oUsuario = db.Usuarios.Find(model.IdPersona);
                        // if (oUsuario.PreguntaA == model.PreguntaA && oUsuario.RespuestaA == Encrypt.GetSHA256(model.RespuestaA) &&
                        //    oUsuario.PreguntaB == model.PreguntaB && oUsuario.RespuestaB == Encrypt.GetSHA256(model.RespuestaB))
                        oUsuario.Password = Encrypt.GetSHA256(model.Password);
                        db.Entry(oUsuario).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        db.SaveChanges();
                        transaction.Commit();
                        oRespuesta.Exito = 1;
                        oRespuesta.Mensaje = "Se modifico correctamente";
                        return oRespuesta;
                    }
                    catch (Exception ex)
                    {
                        oRespuesta.Exito = 0;
                        oRespuesta.Mensaje = ex.Message;
                        return oRespuesta;
                    }
                }
            }
        }

        //modificacion de roles
        //public void Edit(RolRequest model)
        //{
        //    using (SistemaVacunacionContext db = new SistemaVacunacionContext())
        //    {
        //        using (var transaction = db.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                Usuario oUsuario = db.Usuario.Find(model.IdUsuario);
        //                oUsuario.IdRol = model.IdRol;
        //                db.Entry(oUsuario).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        //                db.SaveChanges();
        //                transaction.Commit();
        //            }
        //            catch (Exception)
        //            {
        //                transaction.Rollback();
        //                throw new Exception("Ocurrio un error al modificar el rol del usuario");
        //            }
        //        }
        //    }
        //}




    }
}
