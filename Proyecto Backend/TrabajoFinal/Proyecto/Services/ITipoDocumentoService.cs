﻿using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public interface ITipoDocumentoService
    {
        Respuesta getTipoDocumento();
    }
}
