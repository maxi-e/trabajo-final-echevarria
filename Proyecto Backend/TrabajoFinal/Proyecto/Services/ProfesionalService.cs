﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Proyecto.Models;
using Proyecto.Models.Request;
using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public class ProfesionalService : IProfesionalServices
    {
        public void Edit(ProfesionalRequest oModelProfesional)
        {
            using (TrabajoFinalContext db = new TrabajoFinalContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        //Modifica los datos del profesional   
                        Profesional oProfesional = db.Profesionals.Where(x=>x.IdUsuario == oModelProfesional.IdUsuario).First();
                        // oProfesional.IdUsuario = db.Usuarios.ToList().LastOrDefault().IdUsuario;
                        oProfesional.Matricula = oModelProfesional.Matricula;
                        oProfesional.Especialidad = oModelProfesional.Especialidad;
                        //oProfesional.Estado = oModelProfesional.Estado;


                        db.Entry(oProfesional).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        db.SaveChanges();

                        Usuario oUsuario = db.Usuarios.Where(x => x.IdUsuario == oModelProfesional.IdUsuario).FirstOrDefault();

                        //Modifica los datos de la persona
                        if (oModelProfesional.Persona != null)
                        {
                            Models.Persona oPersona = db.Personas.Where(x => x.IdPersona == oUsuario.IdPersona).FirstOrDefault();
                            oPersona.Dni = oModelProfesional.Persona.Dni;
                            oPersona.Apellido = oModelProfesional.Persona.Apellido;
                            oPersona.Nombre = oModelProfesional.Persona.Nombre;
                            oPersona.Nacionalidad = oModelProfesional.Persona.Nacionalidad;
                            oPersona.Telefono = oModelProfesional.Persona.Telefono;
                            oPersona.Estado = oModelProfesional.Persona.Estado;
                            oPersona.FechaNacimiento = oModelProfesional.Persona.FechaNacimiento;
                            oPersona.Sexo = oModelProfesional.Persona.Sexo;
                            db.Entry(oPersona).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                            db.SaveChanges();
                            
                            if(oModelProfesional.Persona.Domicilio != null)
                            {
                                ////Modifica los datos del domicilio
                                Models.Domicilio oDomicilio = db.Domicilios.Where(x => x.IdDomicilio == oPersona.IdDomicilio).FirstOrDefault();
                                oDomicilio.Nro = oModelProfesional.Persona.Domicilio.Nro;
                                oDomicilio.Calle = oModelProfesional.Persona.Domicilio.Calle;
                                oDomicilio.Localidad = oModelProfesional.Persona.Domicilio.Localidad;
                                oDomicilio.Provincia = oModelProfesional.Persona.Domicilio.Provincia;
                                db.Entry(oDomicilio).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw new Exception("Ocurrio un error al editar los datos");
                    }
                }
            }
        }

        public Respuesta GetProfesional()
        {

            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var lst = db.Profesionals
                                            .Include(x => x.IdUsuarioNavigation)
                                            .ThenInclude(x=>x.IdPersonaNavigation)
                                            .ThenInclude(x => x.IdDomicilioNavigation)
                                            .ToList();//.Where(x => x.Estado == 1).OrderByDescending(x => x.IdPaciente).ToList();.OrderByDescending(x => x.IdProfesional).ToList();

                    MapperConfiguration config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Domicilio, DomicilioResponse>();
                        cfg.CreateMap<Persona, PersonaResponse>();
                        cfg.CreateMap<Profesional, ProfesionalResponse>()
                        .ForMember(dest => dest.Persona, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation))
                        .ForPath(dest => dest.Persona.Domicilio, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation.IdDomicilioNavigation));
                    });

                    var mapper = new Mapper(config);
                    List<ProfesionalResponse> profesionalRespose = mapper.Map<List<ProfesionalResponse>>(lst);

                    oRespuesta.Data = profesionalRespose;
                    oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }

        public Respuesta GetProfesionalId(int idUsuarioProfesional)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var profesional = db.Profesionals
                                            .Include(x => x.IdUsuarioNavigation)
                                            .ThenInclude(x => x.IdPersonaNavigation)
                                            .ThenInclude(x => x.IdDomicilioNavigation)
                                            .Where(x => x.IdUsuario == idUsuarioProfesional).First();//.Find(idProfesional);

                    MapperConfiguration config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<Domicilio, DomicilioResponse>();
                        cfg.CreateMap<Persona, PersonaResponse>();
                        cfg.CreateMap<Profesional, ProfesionalResponse>()
                        .ForMember(dest => dest.Persona, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation))
                        .ForPath(dest => dest.Persona.Domicilio, conf => conf.MapFrom(ol => ol.IdUsuarioNavigation.IdPersonaNavigation.IdDomicilioNavigation));
                    });

                    var mapper = new Mapper(config);
                    ProfesionalResponse profesionalRespose = mapper.Map<ProfesionalResponse>(profesional);
                    oRespuesta.Exito = 1;
                    oRespuesta.Data = profesionalRespose;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Exito = 0;
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }
    }
}
