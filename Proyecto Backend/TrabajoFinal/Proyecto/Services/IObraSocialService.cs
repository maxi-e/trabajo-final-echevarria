﻿using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public interface IObraSocialService
    {
        Respuesta GetObraSocial();
        Respuesta GetObraSocialId(int idObraSocial);
    }
}
