﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Models.Request;
using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public interface IDocumentoService
    {
        Task<Respuesta> GuardarDocumento(DocumentoRequest documentoRequest);
        Respuesta getDocumentos(int idUsuario, int idTipoDocumento);
        Respuesta getArchivo(int idArchivo);
        FileContentResult getSoloArchivo(int idArchivo);
    }
}
