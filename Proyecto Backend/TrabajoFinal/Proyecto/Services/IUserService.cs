﻿using Proyecto.Models.Request;
using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public interface IUserService
    {
        UserResponse Auth(AuthRequest model);
        Respuesta RegistrarMedico(UsuarioRequest model);
        Respuesta RegistrarPaciente(UsuarioRequest model);
        String validarCorreo(string email);
        Respuesta modificarContraseña(AuthRequest model);
        //void Edit(RolRequest model);
    }
}
