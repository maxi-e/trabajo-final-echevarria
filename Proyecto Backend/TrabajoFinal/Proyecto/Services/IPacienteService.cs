﻿using Proyecto.Models.Request;
using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public interface IPacienteService
    {
       // Respuesta Add(PacienteRequest request);
        Respuesta GetPacientes(int idUsuarioProfesional);
        Respuesta GetPacienteId(int idPaciente);
        Respuesta GetProfesionalesAutorizados(int idUsuarioPaciente);
        Respuesta AddAutorizacion(AutorizacionRequest autorizacionRequest);
        Respuesta removerAutorizacion(AutorizacionRequest autorizacionRequest);
        void Edit(PacienteRequest oModelPaciente);
    }
}
