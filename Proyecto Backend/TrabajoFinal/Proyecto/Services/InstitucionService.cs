﻿using AutoMapper;
using Proyecto.Models;
using Proyecto.Models.Response;

namespace Proyecto.Services
{
    public class InstitucionService : IInstitucionService
    {
        public Respuesta GetInstitucion() {
           
            Respuesta oRespuesta = new Respuesta();
            try
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Institucion, InstitucionResponse>()
                );

                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    List<Institucion> lst = db.Institucions.ToList();

                    var mapper = new Mapper(config);
                    List<InstitucionResponse> institucionesRespose = mapper.Map<List<InstitucionResponse>>(lst);

                    oRespuesta.Data = institucionesRespose;
                    oRespuesta.Exito = 1;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return oRespuesta;
        }
    }
}
