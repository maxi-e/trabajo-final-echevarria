﻿using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Models;

namespace Proyecto.Services
{
    public interface IPersonaService
    {
        Respuesta Add(PersonaRequest oModel);
    }
}
