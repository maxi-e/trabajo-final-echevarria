﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Proyecto.Models;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Services;
using System.Data;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentoController : ControllerBase
    {

        private IDocumentoService _documentoService;

        public DocumentoController(IDocumentoService documentoService)
        {
            this._documentoService = documentoService;
        }


        // GET: api/<DocumentoController>
        [HttpGet]
        public IActionResult Get(int idUsuario, int idTipoDocumento)
        {
            return Ok(_documentoService.getDocumentos(idUsuario,idTipoDocumento));
        }

        // GET api/<DocumentoController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet("archivo/{idArchivo}")]
        //[Authorize(Roles = "Admin")]
        public IActionResult GetArchivo(int idArchivo)
        {
            return Ok(_documentoService.getArchivo(idArchivo));
        }

        [HttpGet("soloarchivo/{idArchivo}")]
        //[Authorize(Roles = "Admin")]
        public FileContentResult GetSoloArchivo(int idArchivo)
        {
            return _documentoService.getSoloArchivo(idArchivo);
        }

        [HttpPost]
        public Task<Respuesta> addDocumento([FromForm] DocumentoRequest documentoRequest)
        {
            
            var res = _documentoService.GuardarDocumento(documentoRequest);
            return res;
        }
        //[HttpPost]
        //public IActionResult addDocumento([FromForm] DocumentoRequest documentoRequest)
        //{

        //    var res = _documentoService.GuardarDocumento(documentoRequest);
        //    if (res.Result.Exito == 1)
        //        return Ok(res);
        //    else
        //        return BadRequest(res);
        //}

        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<DocumentoController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
