﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InstitucionController : ControllerBase
    {
        
        private IInstitucionService _institucionService;

        public InstitucionController(IInstitucionService institucionService)
        {
            this._institucionService = institucionService;
        }

        // GET: api/<InstitucionController>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_institucionService.GetInstitucion());
        }

        // GET api/<InstitucionController>/5
        [HttpGet("{id}")] 
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<InstitucionController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<InstitucionController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<InstitucionController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
