﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Models;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }


        [HttpGet("{IdUsuario}")]
        public IActionResult GetUsuarioId(int IdUsuario)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                using (TrabajoFinalContext db = new TrabajoFinalContext())
                {
                    var lst = db.Usuarios.Find(IdUsuario);
                    oRespuesta.Exito = 1;
                    oRespuesta.Data = lst;
                }
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return Ok(oRespuesta);
        }

        [HttpPost("login")]
        public IActionResult Autentificar([FromBody] AuthRequest model)
        {

            Respuesta respuesta = new Respuesta();
            var userresponse = _userService.Auth(model);
            if (userresponse == null)
            {
                respuesta.Exito = 0;
                respuesta.Mensaje = "Usuario o contraseña incorrecta";
                //return BadRequest(respuesta);
                return Ok(respuesta);
            }
            respuesta.Exito = 1;
            respuesta.Data = userresponse;
            return Ok(respuesta);
        }

        [HttpPost("registrarMedico")]
        public IActionResult Registrar([FromBody] UsuarioRequest oModel)
        {
            return Ok(_userService.RegistrarMedico(oModel));
        }

        [HttpPost("registrarPaciente")]
        public IActionResult RegistrarPaciente([FromBody] UsuarioRequest oModel)
        {
            return Ok(_userService.RegistrarPaciente(oModel));
        }

        [HttpPost("validarCorreo")]
        public IActionResult validarCorreo([FromBody] EmailRequest email)
        {

            Respuesta respuesta = new Respuesta();
            var userResponse = _userService.validarCorreo(email.Email);
            if (userResponse != null)
            {
                respuesta.Exito = 1;
                respuesta.Data = userResponse;
                return Ok(respuesta);
            }
            respuesta.Exito = 0;
            respuesta.Data = null;
            return Ok(respuesta);
        }

        [HttpPut("changePassword")]
        public IActionResult Edit(AuthRequest oModel)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                _userService.modificarContraseña(oModel);
                oRespuesta.Exito = 1;
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return Ok(oRespuesta);
        }

    }
}
