﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Models;
using Proyecto.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ObraSocialController : ControllerBase
    {
        private IObraSocialService _obraSocialService;

        public ObraSocialController(IObraSocialService obraSocialService)
        {
            this._obraSocialService = obraSocialService;
        }
        // GET: api/<ObraSocialController>
        [HttpGet]
        public IActionResult GetObraSocial()
        {
            return Ok(_obraSocialService.GetObraSocial());
        }

        // GET api/<ObraSocialController>/5
        [HttpGet("{idObraSocial}")]
        public IActionResult GetPacienteId(int idObraSocial)
        {
            return Ok(_obraSocialService.GetObraSocialId(idObraSocial));
        }
        // POST api/<ObraSocialController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT api/<ObraSocialController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/<ObraSocialController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
