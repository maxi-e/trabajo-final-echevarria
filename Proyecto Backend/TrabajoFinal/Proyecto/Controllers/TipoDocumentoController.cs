﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoDocumentoController : ControllerBase
    {
        private ITipoDocumentoService _tipoDocumentoService;

        public TipoDocumentoController(ITipoDocumentoService tipoDocumentoService)
        {
            this._tipoDocumentoService = tipoDocumentoService;
        }

        // GET: api/<InstitucionController>
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(_tipoDocumentoService.getTipoDocumento());
        }

        // GET api/<TipoDocumentoController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<TipoDocumentoController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<TipoDocumentoController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<TipoDocumentoController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
