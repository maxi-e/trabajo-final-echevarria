﻿using Microsoft.AspNetCore.Mvc;
using Proyecto.Models.Response;
using Proyecto.Models;
using Proyecto.Services;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using Proyecto.Models.Request;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfesionalController : ControllerBase
    {
        private IProfesionalServices _profesionalService;

        public ProfesionalController(IProfesionalServices profesionalService)
        {
            this._profesionalService = profesionalService;
        }
        // GET: api/<ProfesionalController>
        [HttpGet]
        //[Authorize(Roles = "Admin")]
        public IActionResult Get()
        {
            return Ok(_profesionalService.GetProfesional());
        }

        // GET api/<ProfesionalController>/5
        [HttpGet("{idProfesional}")]
        //[Authorize(Roles = "Admin, Profesional")]
        public IActionResult GetProfesionalId (int idProfesional)
        {
            return Ok(_profesionalService.GetProfesionalId(idProfesional));            
        }


        // PUT api/<ProfesionalController>/5
        [HttpPut]
      //  [Authorize(Roles = "Admin, Profesional")]
        public IActionResult Edit([FromBody]ProfesionalRequest oModel)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                _profesionalService.Edit(oModel);
                oRespuesta.Exito = 1;
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return Ok(oRespuesta);
        }

        
    }
}
