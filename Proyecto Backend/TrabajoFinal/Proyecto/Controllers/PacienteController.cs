﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;
using Microsoft.IdentityModel.Tokens;
using Proyecto.Models;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Services;
using System.IdentityModel.Tokens.Jwt;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class PacienteController : ControllerBase
    {
        private IPacienteService _pacienteService;

        public PacienteController(IPacienteService pacienteService)
        {
            this._pacienteService = pacienteService;
        }
        // GET: api/<PacienteController>
        [HttpGet]
        public IActionResult Get([FromHeader] string Authorization)
        { 
            var Token = new JwtSecurityToken(Authorization.Remove(0, 7));//Elimino el Bearer de la cadena para quedarme unicamente con el token
            string idUsuarioPaciente= Token.Payload["nameid"].ToString();
            return Ok(_pacienteService.GetPacientes(int.Parse(idUsuarioPaciente)));
        }

        // GET: api/<PacienteController>
        [HttpGet("profesionalesautorizados")]
        public IActionResult GetProfesionalesAutorizados([FromHeader] string Authorization)
        {
            var Token = new JwtSecurityToken(Authorization.Remove(0, 7));//Elimino el Bearer de la cadena para quedarme unicamente con el token
            string idUsuarioProfesional = Token.Payload["nameid"].ToString();
            return Ok(_pacienteService.GetProfesionalesAutorizados(int.Parse(idUsuarioProfesional)));
        }

        // GET api/<PacienteController>/5
        [HttpGet("{idUsuario}")]
        public IActionResult GetPacienteId(int idUsuario)
        {
            return Ok(_pacienteService.GetPacienteId(idUsuario));
        }

        //POST api/<PacienteController>
        [HttpPost("addAutorizacion")]
        public IActionResult Add([FromBody]AutorizacionRequest autorizacionRequest)
        {
            Respuesta oRespuesta = new Respuesta();
            try
            {
                oRespuesta = _pacienteService.AddAutorizacion(autorizacionRequest);
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
                oRespuesta.Exito = 0;
            }
            return Ok(oRespuesta);
        }

        // PUT api/<PacienteController>/5
        [HttpPut("removerAutorizacion")]
        public IActionResult Edit(AutorizacionRequest oModel)
        {

            Respuesta oRespuesta = new Respuesta();
            try
            {
                oRespuesta = _pacienteService.removerAutorizacion(oModel);
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return Ok(oRespuesta);
        }

        // PUT api/<PacienteController>/5
        [HttpPut]
        public IActionResult Edit(PacienteRequest oModel)
        {

            Respuesta oRespuesta = new Respuesta();
            try
            {
                _pacienteService.Edit(oModel);
                oRespuesta.Exito = 1;
            }
            catch (Exception ex)
            {
                oRespuesta.Mensaje = ex.Message;
            }
            return Ok(oRespuesta);
        }

        // DELETE api/<PacienteController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
