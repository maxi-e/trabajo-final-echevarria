﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Web;
using Proyecto.Models;
using Proyecto.Models.Request;
using Proyecto.Models.Response;
using Proyecto.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Proyecto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonaController : ControllerBase
    {
        private IPersonaService _persona;
        readonly ITokenAcquisition tokenAcquisition;
        public PersonaController(IPersonaService persona, ITokenAcquisition tokenAcquisition)
        {
            this._persona = persona;
            this.tokenAcquisition = tokenAcquisition;
        }
        // GET: api/<PersonaController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<PersonaController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<PersonaController>
        [HttpPost]
        public IActionResult Post([FromBody] PersonaRequest oModel)
        {
             return Ok(_persona.Add(oModel));
        }

        // PUT api/<PersonaController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<PersonaController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
